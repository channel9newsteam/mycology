package davis.mushroom.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AppEventsLogger;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.widget.FacebookDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.drive.Contents;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.MetadataChangeSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ViewDataActivity extends FragmentActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    // Constants
    public static final String ERR_NO_NETWORK = "No network connection.";
    private static final String TAG = "ViewDataActivity";
    private static final int REQUEST_CODE_CREATOR = 2;
    private static final int REQUEST_CODE_RESOLUTION = 3;
    private static final long WAITING_TIME = 10;
    private static final TimeUnit WAITING_TIMESCALE = TimeUnit.SECONDS;

    // Query positions
    private static final int DB_UNIQUE_ID_POS = 0;
    private static final int DB_DATE_POS = 1;
    private static final int DB_TYPE_POS = 2;
    private static final int DB_GPS_POS = 3;
    private static final int DB_MEDIA_POS = 4;
    private static final int DB_NOTES_POS = 5;

    // Instance Variables
    private List listOfFinds;
    public ListedFindsAdapter listAdapter;
    private Spinner sortby;
    private boolean canPresentShareDialog;
    private boolean canPresentShareDialogWithPhotos;
    private UiLifecycleHelper uiHelper;
    private FungusDb db;
    private ArrayList<ArrayList<String>> ids;
    private int sortIndex;


    // Google Drive
    private CreateFolderFileAsyncTask createFolderFileAsync;
    private GoogleApiClient mGoogleApiClient;
    private LinkedList<String> googleDriveQueue;

    // Remote Database
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);


        //getActionBar().setDisplayHomeAsUpEnabled(true);
        db = new FungusDb(getApplicationContext());
        sortIndex = 0;
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
        // Can we present the share dialog for regular links?
        canPresentShareDialog = FacebookDialog.canPresentShareDialog
                (this, FacebookDialog.ShareDialogFeature.SHARE_DIALOG);
        // Can we present the share dialog for photos?
        canPresentShareDialogWithPhotos = FacebookDialog.canPresentShareDialog
                (this, FacebookDialog.ShareDialogFeature.PHOTOS);

        googleDriveQueue = new LinkedList<String>();
        sortby = (Spinner) findViewById(R.id.sortby_spinner);
        final ArrayAdapter<String> sortbyAdapter = new ArrayAdapter<String> (this,
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.sortby_arrays));
        sortbyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortby.setAdapter(sortbyAdapter);
        sortby.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println(sortby.getSelectedItemId());
                sortIndex = i;
                readInFinds();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                readInFinds();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();

        if (!GlobalVariables.noReset) { // reset saved data
            GlobalVariables.savedFolder = null;
        }
        GlobalVariables.noReset = true;
        readInFinds();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
            Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data, dialogCallback);

        // Google Drive activity result
        if (requestCode == REQUEST_CODE_CREATOR) {
            if (resultCode == RESULT_OK) {
                Log.i(TAG, "File successfully saved.");
                //mBitmapToSave = null;
                // Just start the camera again for another photo.
                //startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE),
                //        REQUEST_CODE_CAPTURE_IMAGE);
            }
        }
    }

    @Override
    public void onPause() {
        uiHelper.onPause();

        super.onPause();
    }

    @Override
    public void onDestroy() {
        uiHelper.onDestroy();
        super.onDestroy();
        GlobalVariables.checkedIds.clear();

        disconnectGoogleConnection();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Called whenever the API client fails to connect.
        Log.i(TAG, "GoogleApiClient connection failed: " + result.toString());
        if (!result.hasResolution()) {
            // show the localized error dialog.
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }
        // The failure has a resolution. Resolve it.
        // Called typically when the app is not yet authorized, and an
        // authorization dialog is displayed to the user.
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
        } catch (IntentSender.SendIntentException e) {
            Log.e(TAG, "Exception while starting resolution activity", e);
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        final String GOOGLE_START = "Commencing Google Drive upload.";
        final String GOOGLE_ERROR = "Error doing Google Drive upload.";
        final String GOOGLE_CONNECTION = "Connected to Google Drive";
        Context context = getApplicationContext();
        Log.i(TAG, "API client connected.");

        if (context != null) {
            Toast toast = Toast.makeText(context, GOOGLE_CONNECTION,
                    Toast.LENGTH_LONG);
            toast.show();
        }

        createFolderFileAsync = new CreateFolderFileAsyncTask(this);

        if (googleDriveQueue.isEmpty()) {
            for (String id : GlobalVariables.checkedIds) {
                googleDriveQueue.add(id);
            }
            createFolderFileAsync.execute();

            if (context != null) {
                Toast toast;
                toast = Toast.makeText(context, GOOGLE_START,
                        Toast.LENGTH_LONG);
                toast.show();
            }
        } else {
            // This is triggered if there is already an upload in progress.
            if (context != null) {
                Toast toast;
                toast = Toast.makeText(context, GOOGLE_ERROR,
                        Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.settings, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

    private void sortFinds() {

    }

    private void readInFinds() {
        listOfFinds = new ArrayList();
        String[] sort = getResources().getStringArray(R.array.sortby_arrays);
        ids = db.getFindsFromDb("UniqueIdentifier", null, sort[sortIndex]);
        for (int i = 0; i < ids.size(); i++) {
            listOfFinds.add(new ListedChildren(ids.get(i).get(0), db));
        }
        listAdapter = new ListedFindsAdapter(this, listOfFinds);
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(listAdapter);
    }

    public void deleteSelected(View view) {
        if (GlobalVariables.checkedIds.isEmpty()) {
            DialogFragment newFragment = new WarningDialog();
            newFragment.show(getFragmentManager(), "Warning");
        } else {
            final Dialog deleteMenu = new Dialog(this);
            deleteMenu.setContentView(R.layout.options_yes_no);
            deleteMenu.setTitle("Delete?");
            Button yes = (Button) deleteMenu.findViewById(R.id.button_yes);
            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (String id : GlobalVariables.checkedIds) {
                        db.deleteFindsFromdb("UniqueIdentifier", id);
                        ids.remove(id);
                        FileOperations.deleteFiles(new File(GlobalVariables.DIR_HIDDEN + id));
                    }
                    readInFinds();
                    deleteMenu.cancel();
                    GlobalVariables.checkedIds.clear();
                }
            });
            Button no = (Button) deleteMenu.findViewById(R.id.button_no);
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteMenu.cancel();
                }
            });
            deleteMenu.show();
        }
    }

    public void exportSelected(View view) {
        if (GlobalVariables.checkedIds.isEmpty()) {
            DialogFragment newFragment = new WarningDialog();
            newFragment.show(getFragmentManager(), "Warning");
        } else {
            final Dialog exportOptions = new Dialog(this);
            exportOptions.setContentView(R.layout.options_export);
            exportOptions.setTitle("Exporting Options");

            ImageView facebookImage = (ImageView) exportOptions.findViewById(R.id.export_facebook);
            facebookImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postStatusUpdate();
                    exportOptions.cancel();
                }
            });
            ImageView twitterImage = (ImageView) exportOptions.findViewById(R.id.export_twitter);
            twitterImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postTweet();
                    exportOptions.cancel();
                }
            });
            ImageView googleImage = (ImageView) exportOptions.findViewById(R.id.export_googledrive);
            googleImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    exportToGoogle();
                    exportOptions.cancel();
                }
            });
            ImageView csvImage = (ImageView) exportOptions.findViewById(R.id.export_csv);
            csvImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (String id : GlobalVariables.checkedIds) {
                        createCsvFile(id);
                    }
                    exportOptions.cancel();
                }
            });
            ImageView remoteImage = (ImageView) exportOptions.findViewById(R.id.export_remote);
            remoteImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    postRemote();
                    exportOptions.cancel();
                }
            });
            exportOptions.show();
        }
    }

    public static boolean isNetworkConnected(Context context) {
        boolean wifiConnected = false;
        boolean dataConnected = false;
        ConnectivityManager cm;
        NetworkInfo wifi;
        NetworkInfo data;

        if (context == null) {
            return false;
        }

        cm = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        data = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi != null) {
            wifiConnected = wifi.isConnected();
        }

        if (data != null) {
            dataConnected = data.isConnected();
        }

        return (wifiConnected || dataConnected);
    }

    private void postRemote() {
        final Dialog continueExport = new Dialog(this);
        continueExport.setContentView(R.layout.options_yes_no);
        continueExport.setTitle("Exporting to Remote Database!");
        TextView tv = (TextView) continueExport.findViewById(R.id.message);
        tv.setText("You're about to export to the remote database. By doing so, " +
                "you will be sharing your find with all users of this application. " +
                "Are you sure you want to continue?");
        Button yes = (Button) continueExport.findViewById(R.id.button_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getApplicationContext();

                if (context != null) {
                    Toast toast = Toast.makeText(context,
                            "Exporting", Toast.LENGTH_LONG);
                    exportIntoDb();
                    toast.show();
                    continueExport.cancel();
                }
            }
        });
        Button no = (Button) continueExport.findViewById(R.id.button_no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueExport.cancel();
            }
        });
        continueExport.show();
    }

    private void postTweet() {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        String text = "No internet connection";

        // Check network status
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        // Create intent using ACTION_VIEW and a normal Twitter url if network available
        if(isConnected) {
            String tweetUrl =
                    String.format("https://twitter.com/intent/tweet?text=%s",
                            urlEncode("Look at what I found!"));
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

            // Narrow down to official Twitter app, if available:
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info : matches) {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                    intent.setPackage(info.activityInfo.packageName);
                }
            }
            startActivity(intent);
        } else {
            Toast.makeText(context, text, duration).show();
        }
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            Log.wtf("Unsupported Encoding Execution", "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    private void createCsvFile(String uniqueIdentifier) {
        ArrayList<ArrayList<String>> tuples = db.getFindsFromDb("*", "UniqueIdentifier = " + uniqueIdentifier, null);
        ArrayList<String> columnName = db.getColumnNames();
        File export = new File(GlobalVariables.exportFolder, uniqueIdentifier + File.separator);
        File exportMedia = new File(export, "Media" + File.separator);
        export.mkdirs();
        exportMedia.mkdirs();

        File csvFile;
        BufferedWriter bw = null;
        String delimiter = ",";
        String csvFilePath = export + File.separator + uniqueIdentifier + ".csv"; //need to insert the actual file path here (Please do this Irene!)

        try {
            csvFile = new File(csvFilePath);
            bw = new BufferedWriter(new FileWriter(csvFile));

            for (int i = 0; i < tuples.get(0).size(); i++) {
                if (!columnName.get(i).equals("Media") && !tuples.get(0).get(i).equals("false")
                        && !tuples.get(0).get(i).equals("")) {
                    bw.write(columnName.get(i));
                    bw.write(delimiter);
                    bw.write(tuples.get(0).get(i));
                    bw.newLine();
                    bw.newLine();
                } else if (columnName.get(i).equals("Media")) {
                    FileOperations.copyFiles(new File(tuples.get(0).get(i)), exportMedia);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    // Facebook requires the facebook application to be installed on the device.
    private interface GraphObjectWithId extends GraphObject {
        String getId();
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            //onSessionStateChange(session, state, exception);
        }
    };

    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall,
                Exception error, Bundle data) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall,
                Bundle data) {
            Log.d("HelloFacebook", "Success!");
        }
    };

    private boolean hasPublishPermission() {
        Session session = Session.getActiveSession();
        return session != null && session.getPermissions().contains("publish_actions");
    }

    private FacebookDialog.ShareDialogBuilder
            createShareDialogBuilderForLink() {
        final String DESCRIPTION = "The 'Hello Facebook' sample application " +
                "showcases simple Facebook integration";
        return new FacebookDialog.ShareDialogBuilder(this)
                .setName("Hello Facebook")
                .setDescription(DESCRIPTION)
                .setLink("http://developers.facebook.com/android");
    }

    private FacebookDialog.PhotoShareDialogBuilder
            createShareDialogBuilderForPhoto(Bitmap... photos) {
        return new FacebookDialog.PhotoShareDialogBuilder(this)
                .addPhotos(Arrays.asList(photos));
    }

    private void postStatusUpdate() {
        Context context;
        Session session;
        ArrayList<Bitmap> bitmapArrayList;

        Log.i(TAG, "postStatusUpdate");

        context = getApplicationContext();

        // Detect any errors in getting the network connection. If there are any
        // notify the user and leave.
        if (context == null || !isNetworkConnected(context)) {
            if (context != null) {
                Toast toast = Toast.makeText(context, ERR_NO_NETWORK,
                        Toast.LENGTH_LONG);
                toast.show();
            }

            Log.e(TAG, ERR_NO_NETWORK);
            return;
        }

        session = Session.getActiveSession();
        bitmapArrayList = new ArrayList<Bitmap>();
        // Remove below comment when photos are working.
        //checkAndInsertPhotos(bitmapArrayList);
        // Go through checked entries and insert into the database.

        if (!bitmapArrayList.isEmpty()) {
            // Has photos.
            Bitmap[] bitmapArray;

            bitmapArray = new Bitmap[bitmapArrayList.size()];

            // Copy it into the array.
            for (int i = 0; i < bitmapArrayList.size(); i++) {
                bitmapArray[i] = bitmapArrayList.get(i);
            }

            if (canPresentShareDialogWithPhotos) {
                FacebookDialog shareDialog = createShareDialogBuilderForPhoto(bitmapArray).build();
                uiHelper.trackPendingDialogCall(shareDialog.present());
            } else if (hasPublishPermission()) {
                Request request =
                        Request.newUploadPhotoRequest(Session.getActiveSession(),
                        bitmapArray[0], new Request.Callback() {
                    @Override
                    public void onCompleted(Response response) {
                        showPublishResult(getString(R.string.photo_post), response.getGraphObject(), response.getError());
                    }
                });
                request.executeAsync();
            }
        } else {
            // Has no photos
            if (canPresentShareDialog) {
                // Executed if the user has the Facebook app on their phone.
                Log.i(TAG, "canPresentShareDialog");
                FacebookDialog shareDialog = createShareDialogBuilderForLink()
                        .build();
                uiHelper.trackPendingDialogCall(shareDialog.present());
            } else if (hasPublishPermission()) {
                // If they do not then use the internet session.
                Log.i(TAG, "hasPublishPermission()");
                if (session == null || !session.isOpened()) {
                    Log.e(TAG, "Session is invalid");
                    return;
                }

                final String message = getString(R.string.status_update, null,
                        (new Date().toString()));
                Request request = Request
                        .newStatusUpdateRequest(Session.getActiveSession(), message,
                                null, null, new Request.Callback() {
                                    @Override
                                    public void onCompleted(Response response) {
                                        showPublishResult(message,
                                                response.getGraphObject(),
                                                response.getError());
                                    }
                                }
                        );
                request.executeAsync();
            }
        }
    }

    /*
    private void checkAndInsertPhotos(ArrayList<Bitmap> bitmapArrayList) {
        final String SELECT = "Media";
        if (bitmapArrayList == null) {
            // critical null pointer error.
            return;
        }

        // Go through all the checked entries and insert their images into the
        // array list.
        for (String s : GlobalVariables.checkedIds) {
            // Check s for media files and insert into bitmapArrayList.
            ArrayList<ArrayList<String>> tuple;
            ArrayList<Bitmap> bitmapArray;

            tuple = db.getFindsFromDb(SELECT, "UniqueIdentifier = " + s, null);
            bitmapArray = getAllPhotosInMediaDirectory(tuple.get(0).get(0));

            for (Bitmap b : bitmapArray) {
                bitmapArrayList.add(b);
            }
        }
    }
    */

    private ArrayList<Bitmap> getAllPhotosInMediaDirectory(String path) {
        ArrayList<Bitmap> bitmapArray = new ArrayList<Bitmap>();
        File mediaDirectory = new File(path);
        File[] mediaList = mediaDirectory.listFiles();

        if (mediaList != null) {
            for (File f : mediaList) {
                if (f.getName().contains(".jpg") || f.getName().contains(".JPG")) {
                    Bitmap b = MediaFunctions.getPhotoBitmap(f);
                    bitmapArray.add(b);
                }
            }
        }

        return bitmapArray;
    }

    private void showPublishResult(String message, GraphObject result,
            FacebookRequestError error) {
        String title;
        String alertMessage;

        if (error == null) {
            title = getString(R.string.success);
            String id = result.cast(GraphObjectWithId.class).getId();
            alertMessage = getString(R.string.successfully_posted_post, message,
                    id);
        } else {
            title = getString(R.string.error);
            alertMessage = error.getErrorMessage();
        }

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(alertMessage)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    // Google Drive

    private void enableGoogleConnection() {
        // Call the 'activateApp' method to log an app event for use in analytics and advertising reporting.  Do so in
        // the onResume methods of the primary Activities that an app may be launched into.
        AppEventsLogger.activateApp(this);

        if (mGoogleApiClient == null) {
            // Create the API client and bind it to an instance variable.
            // We use this instance as the callback for connection and connection
            // failures.
            // Since no account name is passed, the user is prompted to choose.
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Drive.API)
                    .addScope(Drive.SCOPE_FILE)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    private void disconnectGoogleConnection() {
        // Check if the api client is connected.
        if (mGoogleApiClient != null) {
            // If the api client is connected, disconnect and set to null for a check later.
            mGoogleApiClient.disconnect();
            mGoogleApiClient = null;
            createFolderFileAsync = null;
            Log.i(TAG, "Google connection disconnected");
        }
    }

    private void exportToGoogle() {
        final Context context = getApplicationContext();
        final String GOOGLE_DRIVE_IN_PROG =
                "A Google Drive upload is already in progress.";
        final String GOOGLE_DRIVE_UPLOAD
                = "Connecting to Google Drive and uploading...";

        if (mGoogleApiClient != null) {
            if (context != null) {
                Toast toast = Toast.makeText(context, GOOGLE_DRIVE_IN_PROG,
                        Toast.LENGTH_LONG);
                toast.show();
            }
            return;
        }

        if (createFolderFileAsync != null) {
            // The createFolderFileAsync is not null, we may not have set it
            // to null after disconnecting the connection, try again.
            createFolderFileAsync = null;
        }

        enableGoogleConnection();
        if (context != null) {
            Toast toast = Toast.makeText(context, GOOGLE_DRIVE_UPLOAD,
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public class CreateFolderFileAsyncTask extends ApiClientAsyncTask<Void, Void, Void> {
        private DriveId findFolderDriveId;
        private DriveId findMediaFolderDriveId;

        public CreateFolderFileAsyncTask(ViewDataActivity context) {
            super(context);
        }

        @Override
        protected Void doInBackgroundConnected(Void... arg0) {
            // (1) get id from queue
            // (2) parse and generate csv file
            // (3) generate csv file on google drive
            // (4) upload media files into google drive.

            while (!googleDriveQueue.isEmpty()) {
                ArrayList<ArrayList<String>> tuples;
                ArrayList<String> columnName;
                DriveFolder.DriveFolderResult folderResult;
                MetadataChangeSet changeSet;
                String id, mediaFolderPath;
                StringBuilder sb = new StringBuilder();

                id = googleDriveQueue.getFirst();
                tuples=db.getFindsFromDb("*", "UniqueIdentifier = " + id, null);
                columnName = db.getColumnNames();

                // Create folder
                changeSet = new MetadataChangeSet.Builder().setTitle("Fungus_"
                        + id).build();

                // Create the fungus folder.
                folderResult = Drive.DriveApi.getRootFolder(mGoogleApiClient)
                        .createFolder(mGoogleApiClient, changeSet)
                        .await(WAITING_TIME, WAITING_TIMESCALE);
                if (!folderResult.getStatus().isSuccess()) {
                    // We failed
                    Log.e(TAG, "Could not create google drive fungus folder.");
                    disconnectGoogleConnection();
                    return null;
                }

                // Get the id for the fungus folder we created.
                findFolderDriveId = folderResult.getDriveFolder().getDriveId();

                changeSet = new MetadataChangeSet.Builder().setTitle("Media")
                        .build();
                // Create media folder.
                folderResult = Drive.DriveApi.getFolder(mGoogleApiClient, findFolderDriveId)
                        .createFolder(mGoogleApiClient, changeSet)
                        .await(WAITING_TIME, WAITING_TIMESCALE);
                if (!folderResult.getStatus().isSuccess()) {
                    // We failed
                    Log.e(TAG, "Could not create google drive media folder.");
                    disconnectGoogleConnection();
                    return null;
                }

                // Get the media folder id.
                findMediaFolderDriveId = folderResult.getDriveFolder()
                        .getDriveId();

                // Create the string for the query and get the media directory.
                mediaFolderPath = getMediaFolderPathAndCSVString(tuples,
                        columnName, sb);

                // Create the csv file and add the data to it.
                uploadCsvToGoogleDrive(id, sb.toString());

                // Upload the files in the media folder.
                if (mediaFolderPath != null && !mediaFolderPath.equals("")) {
                    uploadMediaToGoogleDrive(mediaFolderPath);
                }

                googleDriveQueue.removeFirst();
            }

            disconnectGoogleConnection();
            return null;
        }

        private String getMediaFolderPathAndCSVString(
            ArrayList<ArrayList<String>> tuples, ArrayList<String> columnName,
            StringBuilder sb) {
            String mediaFolderPath = null;

            // Create the string for the query and get the media directory.
            for (int i = 0; i < tuples.get(0).size(); i++) {
                if (!columnName.get(i).equals("Media")
                        && !tuples.get(0).get(i).equals("false")
                        && !tuples.get(0).get(i).equals("")) {
                    sb.append(columnName.get(i));
                    sb.append(',');
                    sb.append(tuples.get(0).get(i));
                    sb.append('\n');
                } else if (columnName.get(i).equals("Media")) {
                    mediaFolderPath = tuples.get(0).get(i);
                }
            }

            return mediaFolderPath;
        }

        private void uploadCsvToGoogleDrive(final String id,
                final String queryResults) {
            final String EXT = ".csv";
            DriveApi.ContentsResult contentsResult;
            DriveFolder.DriveFileResult fileResult;
            Contents originalContents;
            DriveFolder folder;
            OutputStream outputStream;
            String fileName;
            MetadataChangeSet metadataChangeSet;

            contentsResult = Drive.DriveApi.newContents(mGoogleApiClient)
                    .await(WAITING_TIME, WAITING_TIMESCALE);
            if (!contentsResult.getStatus().isSuccess()) {
                // We failed, stop the task and return.
                final String message = "Google Drive could not create new" +
                        " contents.";

                Log.e(TAG, message);
                return;
            }

            Log.i(TAG, "New contents created.");
            originalContents = contentsResult.getContents();
            // Get an output stream for the contents.
            outputStream = originalContents.getOutputStream();

            // Write the bitmap data from it.
            try {
                outputStream.write(queryResults.getBytes());
            } catch (IOException e1) {
                Log.i(TAG, "Unable to write file contents.");
                e1.printStackTrace();
                return;
            }

            fileName = id + EXT;
            // Create the initial metadata - MIME type and title.
            // Note that the user will be able to change the title later.
            Log.i(TAG, "fileName: " + fileName);
            metadataChangeSet = new MetadataChangeSet.Builder()
                    .setTitle(fileName).setMimeType("text/plain").build();
            // Create an intent for the file chooser, and start it.

            Log.i(TAG, "Uploading file to Google Drive");
            folder = Drive.DriveApi.getFolder(mGoogleApiClient,
                    findFolderDriveId);

            fileResult = folder.createFile(mGoogleApiClient, metadataChangeSet,
                    originalContents).await(WAITING_TIME, WAITING_TIMESCALE);
            if (!fileResult.getStatus().isSuccess()) {
                // We failed, stop the task and return.
                final String message = "Google Drive could not create new" +
                        " file.";
                Log.e(TAG, message);
                return;
            }

            Log.i(TAG, "Done with uploading file");
        }

        private void uploadMediaToGoogleDrive(String mediaFolderPath) {
            final String JPG_EXT = ".jpg";
            File mediaFolder = new File(mediaFolderPath);
            File[] filesInMediaFolder = mediaFolder.listFiles();

            if (filesInMediaFolder != null) {
                for (File f : filesInMediaFolder) {
                    if (f.getName().contains(JPG_EXT)) {
                        uploadImageToGoogleDrive(f.getName(), f);
                    }
                }
            }
        }

        private void uploadImageToGoogleDrive(final String fileName,
                final File image) {
            DriveApi.ContentsResult contentsResult;
            DriveFolder.DriveFileResult fileResult;
            Contents originalContents;
            DriveFolder folder;
            OutputStream outputStream;
            MetadataChangeSet metadataChangeSet;

            contentsResult = Drive.DriveApi.newContents(mGoogleApiClient)
                    .await(WAITING_TIME, WAITING_TIMESCALE);
            if (!contentsResult.getStatus().isSuccess()) {
                // We failed, stop the task and return.
                return;
            }

            Log.i(TAG, "New contents created.");
            originalContents = contentsResult.getContents();
            // Get an output stream for the contents.
            outputStream = originalContents.getOutputStream();

            // Write the bitmap data from it.
            try {
                FileInputStream fis = new FileInputStream(image);
                BufferedInputStream bis = new BufferedInputStream(fis);
                byte[] blockArray = new byte[1024];

                while ((bis.read(blockArray, 0, blockArray.length)) >= 0) {
                    outputStream.write(blockArray);
                }
            } catch (IOException e1) {
                Log.i(TAG, "Unable to write file contents.");
                e1.printStackTrace();
                return;
            }
            // Create the initial metadata - MIME type and title.
            // Note that the user will be able to change the title later.
            Log.i(TAG, "image fileName: " + fileName);
            metadataChangeSet = new MetadataChangeSet.Builder()
                    .setMimeType("image/jpeg").setTitle(fileName).build();
            // Create an intent for the file chooser, and start it.

            Log.i(TAG, "Uploading file to Google Drive");
            folder = Drive.DriveApi.getFolder(mGoogleApiClient,
                    findMediaFolderDriveId);
            fileResult = folder.createFile(mGoogleApiClient, metadataChangeSet,
                    originalContents)
                    .await(WAITING_TIME, WAITING_TIMESCALE);
            if (!fileResult.getStatus().isSuccess()) {
                // We failed, stop the task and return.
                return;
            }

            Log.i(TAG, "Done with uploading image");
        }
    }

    // Upload to remote database.
    // Inserting into the remote database.

    private void exportIntoDb() {
        // Fill a queue and dequeue and insert into db
        CreateNewFind newFind;
        LinkedList<String> queue = new LinkedList<String>();

        for (String s : GlobalVariables.checkedIds) {
            queue.add(s);
        }

        // Execute asynchronous task to upload into the remote database.
        newFind = new CreateNewFind(queue);
        newFind.execute();
    }

    private String getFirstImageInMediaDirectory(String fileDirectoryPath) {
        final int REQ_SIZE = 64;
        File fileDirectory = new File(fileDirectoryPath);
        File[] images = fileDirectory.listFiles();

        if (images != null && images.length > 0) {
            byte[] byteArray;
            Bitmap bitmap;
            ByteArrayOutputStream stream;
            String encodedByteArray;

            // Compress the image to 64 x 64 and open up a new stream.
            bitmap = MediaFunctions.getPhotoBitmapBounded(images[0],
                    REQ_SIZE, REQ_SIZE);
            stream = new ByteArrayOutputStream();

            // Place the compressed image into the stream and convert it into a
            // byte array.
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byteArray = stream.toByteArray();
            encodedByteArray = Base64.encodeToString(byteArray, Base64.DEFAULT);

            return encodedByteArray;
        }

        return "null";
    }

    /**
     * Background Async Task to Create new product
     * */
    class CreateNewFind extends AsyncTask<String, String, String> {
        // Instance Variables
        private LinkedList<String> queue;

        public CreateNewFind(final LinkedList<String> queue) {
            this.queue = queue;
        }

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewDataActivity.this);
            pDialog.setMessage("Creating Find..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating find
         * */
        protected String doInBackground(String... args) {
            final int STRING_ARRAY_SIZE = 6;
            final String SELECT = "UniqueIdentifier, Date, Type, GPS, Media, Notes";

            while (!queue.isEmpty()) {
                String id = queue.getFirst();
                ArrayList<ArrayList<String>> tuple = db.getFindsFromDb(SELECT,
                        "UniqueIdentifier = " + id, null);

                if (tuple.get(0).size() >= STRING_ARRAY_SIZE) {
                    String mediaDirectoryPath;
                    String[] attributes = new String[STRING_ARRAY_SIZE];

                    attributes[DB_UNIQUE_ID_POS] = tuple.get(0).get(DB_UNIQUE_ID_POS);
                    attributes[DB_DATE_POS] = tuple.get(0).get(DB_DATE_POS);
                    attributes[DB_TYPE_POS] = tuple.get(0).get(DB_TYPE_POS);
                    attributes[DB_GPS_POS] = tuple.get(0).get(DB_GPS_POS);
                    attributes[DB_NOTES_POS] = tuple.get(0).get(DB_NOTES_POS);

                    mediaDirectoryPath = tuple.get(0).get(DB_MEDIA_POS);

                    // Convert 1st image bitmap into string.
                    attributes[DB_MEDIA_POS] =
                            getFirstImageInMediaDirectory(mediaDirectoryPath);

                    Log.i(TAG, "Uploading " + id + " to remote database");
                    uploadToDb(attributes);
                }

                queue.removeFirst();
            }

            return null;
        }

        private void uploadToDb(final String[] attributes) {
            String id = attributes[DB_UNIQUE_ID_POS];
            String date = attributes[DB_DATE_POS];
            String type = attributes[DB_TYPE_POS];
            String gps = attributes[DB_GPS_POS];
            String media = attributes[DB_MEDIA_POS];
            String notes = attributes[DB_NOTES_POS];

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(FungusDb.UNIQUE_IDENTIFIER, id));
            params.add(new BasicNameValuePair(FungusDb.DATE, date));
            params.add(new BasicNameValuePair(FungusDb.TYPE, type));
            params.add(new BasicNameValuePair(FungusDb.GPS, gps));
            params.add(new BasicNameValuePair(FungusDb.MEDIA, media));
            params.add(new BasicNameValuePair(FungusDb.NOTES, notes));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = JSONParser
                    .makeHttpRequest(FungusDb.URL_CREATE_FIND,
                            JSONParser.POST, params);

            // check for success tag
            try {
                int success = json.getInt(FungusDb.SUCCESS);

                // check log cat for response
                Log.d("Create Response", json.toString());

                if (success == 1) {
                    // Created a new find.
                    Log.i(TAG, "Created a new find.");
                } else {
                    // failed to create the new find
                    Log.e(TAG, "Failed to create a new find in the db");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch( NullPointerException e) {
                e.printStackTrace();
            }
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    /*
                     ListAdapter adapter = new SimpleAdapter(
                            AllProductsActivity.this, productsList,
                            R.layout.list_item, new String[] { TAG_PID,
                            TAG_NAME},
                            new int[] { R.id.pid, R.id.name });
                    // updating listview
                    setListAdapter(adapter);
                    */
                }
            });
            // dismiss the dialog once done
            pDialog.dismiss();
        }
    }
}


