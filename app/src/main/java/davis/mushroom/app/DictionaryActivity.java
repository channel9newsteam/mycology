package davis.mushroom.app;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.PopupMenu;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Garrett on 5/19/14.
 */
public class DictionaryActivity extends FragmentActivity {
    public ExpandableListView exv;

    private ArrayList<String> parentList;
    private ArrayList<ArrayList<String>> childList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary);
        exv = (ExpandableListView) findViewById(R.id.expandableListView);
        parentList = createParentList();
        childList = createChildList();
        exv.setAdapter(new DictionaryExpandableListAdapter(this, parentList, childList));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.settings, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

    private ArrayList<String> createParentList() {
        return new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.mushroom_types)));
    }

    private ArrayList<ArrayList<String>> createChildList() {
        ArrayList<ArrayList<String>> childList = new ArrayList<ArrayList<String>>();
        FungusDb fungusDb = new FungusDb(getApplicationContext());

        for (String type: getResources().getStringArray(R.array.mushroom_types)) {
            childList.add(queryAndReturnScientificNames(fungusDb, type));
        }

        return childList;
    }

    private ArrayList<String> queryAndReturnScientificNames(FungusDb fungusDb, String mushroomType) {
        ArrayList<ArrayList<String>> queryResult = fungusDb.getDbInfo("*", FungusDb.DICTIONARY_TABLE, "Type = \'" + mushroomType + '\'', null);
        ArrayList<String> listToAdd = new ArrayList<String>();
        for (ArrayList<String> page : queryResult) {
            listToAdd.add(page.get(1));
        }
        return listToAdd;
    }

}
