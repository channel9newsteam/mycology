package davis.mushroom.app;

import android.app.DialogFragment;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by quazeenii on 5/13/14.
 */
public class SavedData extends TabActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbing);
        TabHost tabHost=getTabHost();
        TextView local = new TextView(this);
        TextView remote = new TextView(this);

        local.setTextSize(14);
        local.setGravity(Gravity.CENTER);
        local.setTypeface(Typeface.create("sans", Typeface.BOLD));
        remote.setTextSize(14);
        remote.setGravity(Gravity.CENTER);
        remote.setTypeface(Typeface.create("sans", Typeface.BOLD));
        getActionBar().setDisplayHomeAsUpEnabled(true);

        Intent in1 = new Intent(this, ViewDataActivity.class);
        TabHost.TabSpec spec1 = tabHost.newTabSpec("Local");
        local.setText("Local");
        spec1.setIndicator(local);
        spec1.setContent(in1);

        Intent in2 = new Intent(this, ViewRemoteData.class);
        TabHost.TabSpec spec2=tabHost.newTabSpec("Remote");
        remote.setText("Remote");
        spec2.setIndicator(remote);
        spec2.setContent(in2);

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.setCurrentTab(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.\

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.settings, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Auto-generated method stub
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;
                CharSequence text;

                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

}
