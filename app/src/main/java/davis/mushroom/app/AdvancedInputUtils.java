package davis.mushroom.app;

import java.util.ArrayList;

/**
 * Created by Garrett on 4/28/14.
 */
public class AdvancedInputUtils {

    public static ArrayList<ArrayList<String>> createNestedParentList() {
        //There must be the same number of lists in here as in createParentList
        ArrayList<ArrayList<String>> nestedParentList = new ArrayList<ArrayList<String>>();
        nestedParentList.add(createCapNestedParentList());
        nestedParentList.add(createGillsTubesTeethNestedParentList());
        nestedParentList.add(createStalkNestedParentList());
        nestedParentList.add(createVeilsNestedParentList());
        nestedParentList.add(createSporesNestedParentList());
        nestedParentList.add(createHabitNestedParentList());
        return nestedParentList;
    }

    public static ArrayList<ArrayList<ArrayList<String>>> createChildList() {
        //child list must have the same number of items as createParentList().size() produces
        ArrayList<ArrayList<ArrayList<String>>> childList = new ArrayList<ArrayList<ArrayList<String>>>();
        childList.add(createCapChildList());
        childList.add(createGillsTubesTeethChildList());
        childList.add(createStalkChildList());
        childList.add(createVeilsChildList());
        childList.add(createSporesChildList());
        childList.add(createHabitChildList());
        return childList;
    }

    private static ArrayList<ArrayList<String>> createCapChildList() {
        ArrayList<ArrayList<String>> capChildren = new ArrayList<ArrayList<String>>();
        capChildren.add(createCapSizeChildList());
        capChildren.add(createCapShapeChildList());
        capChildren.add(createCapTextureChildList());
        capChildren.add(createCapColorChildList());
        capChildren.add(createCapContextChildList());
        capChildren.add(createCapOtherChildList());
        return capChildren;
    }

    private static ArrayList<ArrayList<String>> createGillsTubesTeethChildList() {
        ArrayList<ArrayList<String>> gillsChildren = new ArrayList<ArrayList<String>>();
        gillsChildren.add(createGillsStemChildList());
        gillsChildren.add(createGillsSpacingChildList());
        gillsChildren.add(createGillsBreadthChildList());
        gillsChildren.add(createGillsColorChildList());
        gillsChildren.add(createGillsMarginChildList());
        gillsChildren.add(createGillsOtherChildList());
        return gillsChildren;
    }

    private static ArrayList<ArrayList<String>> createStalkChildList() {
        ArrayList<ArrayList<String>> stalkChildren = new ArrayList<ArrayList<String>>();
        stalkChildren.add(createStalkLocationChildList());
        stalkChildren.add(createStalkSizeChildList());
        stalkChildren.add(createStalkShapeChildList());
        stalkChildren.add(createStalkTextureChildList());
        stalkChildren.add(createStalkColorChildList());
        stalkChildren.add(createStalkSolidityChildList());
        stalkChildren.add(createStalkConsistencyChildList());
        stalkChildren.add(createStalkOtherChildList());
        return stalkChildren;
    }

    private static ArrayList<ArrayList<String>> createVeilsChildList() {
        ArrayList<ArrayList<String>> veilsChildren = new ArrayList<ArrayList<String>>();
        veilsChildren.add(createVeilsPartialChildList());
        veilsChildren.add(createVeilsUniversalChildList());
        veilsChildren.add(createVeilsOtherChildList());
        return veilsChildren;
    }

    private static ArrayList<ArrayList<String>> createSporesChildList() {
        ArrayList<ArrayList<String>> sporesChildren = new ArrayList<ArrayList<String>>();
        sporesChildren.add(createSporesColorChildList());
        sporesChildren.add(createSporesSizeChildList());
        sporesChildren.add(createSporesShapeChildList());
        sporesChildren.add(createSporesOrnamentationChildList());
        sporesChildren.add(createSporesOtherChildList());
        return sporesChildren;
    }

    private static ArrayList<ArrayList<String>> createHabitChildList() {
        ArrayList<ArrayList<String>> habitChildren = new ArrayList<ArrayList<String>>();
        habitChildren.add(createHabitSubstrateChildList());
        habitChildren.add(createHabitGroupingChildList());
        habitChildren.add(createHabitOtherChildList());
        return habitChildren;
    }

    public static ArrayList<String> createParentList() {
        ArrayList<String> parentList = new ArrayList<String>();
        parentList.add("Cap");
        parentList.add("Gills, Tubes, Or Teeth");
        parentList.add("Stalk");
        parentList.add("Veils");
        parentList.add("Spores");
        parentList.add("Habitat");
        return parentList;
    }

    private static ArrayList<String> createCapNestedParentList() {
        //unprintable characters are used to create unique identifiers
        ArrayList<String> capChildren = new ArrayList<String>();
        capChildren.add("Size\0");
        capChildren.add("Shape\0");
        capChildren.add("Texture\0");
        capChildren.add("Color\0");
        capChildren.add("Context\0");
        capChildren.add("Other\0");
        return capChildren;
    }

    private static ArrayList<String> createGillsTubesTeethNestedParentList() {
        //unprintable characters are used to create unique identifiers
        ArrayList<String> gillsChildren = new ArrayList<String>();
        gillsChildren.add("Attachment To Stem\1");
        gillsChildren.add("Spacing (Gills)\1");
        gillsChildren.add("Breadth\1");
        gillsChildren.add("Color\1");
        gillsChildren.add("Margin\1");
        gillsChildren.add("Other\1");
        return gillsChildren;
    }

    private static ArrayList<String> createStalkNestedParentList() {
        //unprintable characters are used to create unique identifiers
        ArrayList<String> stalkChildren = new ArrayList<String>();
        stalkChildren.add("Location\2");
        stalkChildren.add("Size\2");
        stalkChildren.add("Shape\2");
        stalkChildren.add("Texture\2");
        stalkChildren.add("Color\2");
        stalkChildren.add("Solidity\2");
        stalkChildren.add("Consistency\2");
        stalkChildren.add("Other\2");
        return stalkChildren;
    }

    private static ArrayList<String> createVeilsNestedParentList() {
        //unprintable characters are used to create unique identifiers
        ArrayList<String> veilsChildren = new ArrayList<String>();
        veilsChildren.add("Partial Veil\3");
        veilsChildren.add("Universal Veils\3");
        veilsChildren.add("Other\2");
        return veilsChildren;
    }

    private static ArrayList<String> createSporesNestedParentList() {
        //unprintable characters are used to create unique identifiers
        ArrayList<String> sporesChildren = new ArrayList<String>();
        sporesChildren.add("Color\4");
        sporesChildren.add("Size\4");
        sporesChildren.add("Shape\4");
        sporesChildren.add("Ornamentation\4");
        sporesChildren.add("Other\4");
        return sporesChildren;
    }

    private static ArrayList<String> createHabitNestedParentList() {
        //unprintable characters are used to create unique identifiers
        ArrayList<String> habitChildren = new ArrayList<String>();
        habitChildren.add("Substrate\5");
        habitChildren.add("Grouping\5");
        habitChildren.add("Other\5");
        return habitChildren;
    }

    private static ArrayList<String> createCapSizeChildList() {
        ArrayList<String> capSizeChildList = new ArrayList<String>();
        capSizeChildList.add("Width:");
        capSizeChildList.add("Thickness:");
        return capSizeChildList;
    }

    private static ArrayList<String> createCapShapeChildList() {
        ArrayList<String> capShapeChildList = new ArrayList<String>();
        capShapeChildList.add("General: Conic");
        capShapeChildList.add("General: Campanulate");
        capShapeChildList.add("General: Hemispherical");
        capShapeChildList.add("General: Convex");
        capShapeChildList.add("General: Flat");
        capShapeChildList.add("General: Depressed");
        capShapeChildList.add("General: Infundibuliform");
        capShapeChildList.add("Modifiers: Umbonate");
        capShapeChildList.add("Modifiers: Umbilicate");
        capShapeChildList.add("Margin: Incurved");
        capShapeChildList.add("Margin: Inrolled");
        capShapeChildList.add("Margin: Uplifted");
        capShapeChildList.add("Margin: Appendiculate");
        capShapeChildList.add("Margin: Striate");
        return capShapeChildList;
    }

    private static ArrayList<String> createCapTextureChildList() {
        ArrayList<String> capTextureChildList = new ArrayList<String>();
        capTextureChildList.add("Touch: Dry");
        capTextureChildList.add("Touch: Moist");
        capTextureChildList.add("Touch: Sticky or Tacky");
        capTextureChildList.add("Touch: Slimy");
        capTextureChildList.add("Visual: Smooth");
        capTextureChildList.add("Visual: Fibrous");
        capTextureChildList.add("Visual: Flattened");
        capTextureChildList.add("Visual: Erect");
        capTextureChildList.add("Visual: Scaly");
        capTextureChildList.add("Visual: Warted");
        return capTextureChildList;
    }

    private static ArrayList<String> createCapColorChildList() {
        ArrayList<String> capColorChildList = new ArrayList<String>();
        capColorChildList.add("Coloration:"); //need text entry
        capColorChildList.add("Cap and Edge:"); //need text entry
        capColorChildList.add("Hygrophanous:"); //need text entry
        return capColorChildList;
    }

    private static ArrayList<String> createCapContextChildList() {
        ArrayList<String> capContextChildList = new ArrayList<String>();
        capContextChildList.add("Thick");
        capContextChildList.add("Thin");
        capContextChildList.add("Color:"); //need text entry
        capContextChildList.add("Tough");
        capContextChildList.add("Fragile");
        capContextChildList.add("Odor: Mild");
        capContextChildList.add("Odor: Green Corn");
        capContextChildList.add("Odor: Almond");
        capContextChildList.add("Odor: Phenolic");
        capContextChildList.add("Odor: Spicy");
        capContextChildList.add("Odor: Sewer Gas");
        capContextChildList.add("Odor: Mushroomy");
        capContextChildList.add("Odor: Raddish Like");
        capContextChildList.add("Odor: Farinaceous");
        capContextChildList.add("Taste: Mild");
        capContextChildList.add("Taste: Acrid");
        capContextChildList.add("Taste: Farinaceous");
        capContextChildList.add("Taste: Bitter");
        capContextChildList.add("Taste: Sour");
        return capContextChildList;
    }

    private static ArrayList<String> createCapOtherChildList() {
        ArrayList<String> capOtherChildList = new ArrayList<String>();
        capOtherChildList.add("Notes:"); //need text entry
        return capOtherChildList;
    }

    private static ArrayList<String> createGillsStemChildList() {
        ArrayList<String> gillsStemChildList = new ArrayList<String>();
        gillsStemChildList.add("Free");
        gillsStemChildList.add("Adnexed or Notched");
        gillsStemChildList.add("Adnate");
        gillsStemChildList.add("Decurrent");
        return gillsStemChildList;
    }

    private static ArrayList<String> createGillsSpacingChildList() {
        ArrayList<String> gillsSpacingChildList = new ArrayList<String>();
        gillsSpacingChildList.add("Crowded");
        gillsSpacingChildList.add("Close");
        gillsSpacingChildList.add("Subdistant");
        gillsSpacingChildList.add("Distant");
        return gillsSpacingChildList;
    }

    private static ArrayList<String> createGillsBreadthChildList() {
        ArrayList<String> gillsBreadthChildList = new ArrayList<String>();
        gillsBreadthChildList.add("Broad");
        gillsBreadthChildList.add("Narrow");
        return gillsBreadthChildList;
    }

    private static ArrayList<String> createGillsColorChildList() {
        ArrayList<String> gillsColorChildList = new ArrayList<String>();
        gillsColorChildList.add("Description:"); //need text entry
        return gillsColorChildList;
    }

    private static ArrayList<String> createGillsMarginChildList() {
        ArrayList<String> gillsMarginChildList = new ArrayList<String>();
        gillsMarginChildList.add("Description:"); //need text entry
        return gillsMarginChildList;
    }

    private static ArrayList<String> createGillsOtherChildList() {
        ArrayList<String> gillsOtherChildList = new ArrayList<String>();
        gillsOtherChildList.add("Notes:"); //need text entry
        return gillsOtherChildList;
    }

    private static ArrayList<String> createStalkLocationChildList() {
        ArrayList<String> stalkLocationChildList = new ArrayList<String>();
        stalkLocationChildList.add("Central");
        stalkLocationChildList.add("Eccentric");
        stalkLocationChildList.add("Lateral");
        return stalkLocationChildList;
    }

    private static ArrayList<String> createStalkSizeChildList() {
        ArrayList<String> stalkSizeChildList = new ArrayList<String>();
        stalkSizeChildList.add("Base to Cap Length:");
        stalkSizeChildList.add("Thickness at Cap Connection:");
        return stalkSizeChildList;
    }

    private static ArrayList<String> createStalkShapeChildList() {
        ArrayList<String> stalkShapeChildList = new ArrayList<String>();
        stalkShapeChildList.add("Equal");
        stalkShapeChildList.add("Tapering");
        stalkShapeChildList.add("Clavate");
        stalkShapeChildList.add("Ventricose");
        stalkShapeChildList.add("Rooting");
        return stalkShapeChildList;
    }

    private static ArrayList<String> createStalkTextureChildList() {
        ArrayList<String> stalkTextureChildList = new ArrayList<String>();
        stalkTextureChildList.add("Touch: Dry");
        stalkTextureChildList.add("Touch: Moist");
        stalkTextureChildList.add("Touch: Sticky or Tacky");
        stalkTextureChildList.add("Touch: Slimy");
        stalkTextureChildList.add("Visual: Smooth");
        stalkTextureChildList.add("Visual: Fibrous or Fibrillose");
        stalkTextureChildList.add("Visual: Flattened");
        stalkTextureChildList.add("Visual: Erect");
        stalkTextureChildList.add("Visual: Scaly");
        return stalkTextureChildList;
    }

    private static ArrayList<String> createStalkColorChildList() {
        ArrayList<String> stalkColorChildList = new ArrayList<String>();
        stalkColorChildList.add("Description:"); //needs text entry
        return stalkColorChildList;
    }

    private static ArrayList<String> createStalkSolidityChildList() {
        ArrayList<String> stalkSolidityChildList = new ArrayList<String>();
        stalkSolidityChildList.add("Hollow");
        stalkSolidityChildList.add("Solid");
        return stalkSolidityChildList;
    }

    private static ArrayList<String> createStalkConsistencyChildList() {
        ArrayList<String> stalkConsistencyChildList = new ArrayList<String>();
        stalkConsistencyChildList.add("Fragile");
        stalkConsistencyChildList.add("Cartilaginous");
        stalkConsistencyChildList.add("Pliable");
        stalkConsistencyChildList.add("Brittle");
        stalkConsistencyChildList.add("Soft");
        stalkConsistencyChildList.add("Hard");
        return stalkConsistencyChildList;
    }

    private static ArrayList<String> createStalkOtherChildList() {
        ArrayList<String> stalkOtherChildList = new ArrayList<String>();
        stalkOtherChildList.add("Notes:"); //needs text entry
        return stalkOtherChildList;
    }

    private static ArrayList<String> createVeilsPartialChildList() {
        ArrayList<String> veilsPartialChildList = new ArrayList<String>();
        veilsPartialChildList.add("Texture: Cortina");
        veilsPartialChildList.add("Texture: Membranous");
        veilsPartialChildList.add("Leaves a Ring: Persistent");
        veilsPartialChildList.add("Leaves a Ring: Evanescent");
        veilsPartialChildList.add("Ring Zone (Location): Superior");
        veilsPartialChildList.add("Ring Zone (Location): Median");
        veilsPartialChildList.add("Ring Zone (Location): Inferior");
        veilsPartialChildList.add("Ring Zone (Location): Movable");
        veilsPartialChildList.add("Ring Zone (Texture): Skirtlike");
        veilsPartialChildList.add("Ring Zone (Texture): Bandlike");
        veilsPartialChildList.add("Ring Zone (Texture): Fibrous");
        veilsPartialChildList.add("Ring Zone (Texture): Slimy");
        veilsPartialChildList.add("Ring Zone (Texture): Double");
        veilsPartialChildList.add("Ring Zone (Color): Description:"); //needs text entry
        veilsPartialChildList.add("Ring Zone (Other): Notes:"); //needs text entry
        return veilsPartialChildList;
    }

    private static ArrayList<String> createVeilsUniversalChildList() {
        ArrayList<String> veilsUniversalChildList = new ArrayList<String>();
        veilsUniversalChildList.add("Texture: Skirtlike");
        veilsUniversalChildList.add("Texture: Bandlike");
        veilsUniversalChildList.add("Texture: Fibrous");
        veilsUniversalChildList.add("Texture: Slimy");
        veilsUniversalChildList.add("Color: Description:"); //needs text entry
        veilsUniversalChildList.add("Volva: Description:"); //needs text entry
        return veilsUniversalChildList;
    }

    private static ArrayList<String> createVeilsOtherChildList() {
        ArrayList<String> stalkOtherChildList = new ArrayList<String>();
        stalkOtherChildList.add("Notes:"); //needs text entry
        return stalkOtherChildList;
    }

    private static ArrayList<String> createSporesColorChildList() {
        ArrayList<String> sporesColorChildList = new ArrayList<String>();
        sporesColorChildList.add("Description:"); //needs text entry
        return sporesColorChildList;
    }

    private static ArrayList<String> createSporesSizeChildList() {
        ArrayList<String> sporesSizeChildList = new ArrayList<String>();
        sporesSizeChildList.add("Length:");
        sporesSizeChildList.add("Width:");
        return sporesSizeChildList;
    }

    private static ArrayList<String> createSporesShapeChildList() {
        ArrayList<String> sporesShapeChildList = new ArrayList<String>();
        sporesShapeChildList.add("Globsose");
        sporesShapeChildList.add("Ellipsoid");
        sporesShapeChildList.add("Oblong");
        sporesShapeChildList.add("Ovoid");
        sporesShapeChildList.add("Fusiform or Spindle Shaped");
        sporesShapeChildList.add("Nodulose");
        sporesShapeChildList.add("Cylindric");
        return sporesShapeChildList;
    }

    private static ArrayList<String> createSporesOrnamentationChildList() {
        ArrayList<String> sporesOrnamentationChildList = new ArrayList<String>();
        sporesOrnamentationChildList.add("Warty");
        sporesOrnamentationChildList.add("Partial or Broken Reticulum");
        sporesOrnamentationChildList.add("Reticulate");
        sporesOrnamentationChildList.add("Wrinkled");
        sporesOrnamentationChildList.add("Striate or Grooved");
        return sporesOrnamentationChildList;
    }

    private static ArrayList<String> createSporesOtherChildList() {
        ArrayList<String> sporesOtherChildList = new ArrayList<String>();
        sporesOtherChildList.add("Amyloid");
        sporesOtherChildList.add("Dextrinoid");
        sporesOtherChildList.add("Notes:"); //need text entry
        return sporesOtherChildList;
    }

    private static ArrayList<String> createHabitSubstrateChildList() {
        ArrayList<String> habitSubstrateChildList = new ArrayList<String>();
        habitSubstrateChildList.add("On the Ground");
        habitSubstrateChildList.add("On Wood: Hardwood");
        habitSubstrateChildList.add("On Wood: Pine");
        habitSubstrateChildList.add("On Dung");
        habitSubstrateChildList.add("On Mushrooms");
        return habitSubstrateChildList;
    }

    private static ArrayList<String> createHabitGroupingChildList() {
        ArrayList<String> habitGroupingChildList = new ArrayList<String>();
        habitGroupingChildList.add("Single or Solitary");
        habitGroupingChildList.add("Groups (few together)");
        habitGroupingChildList.add("Gregarious (many together)");
        habitGroupingChildList.add("Cespitose (commom base)");
        habitGroupingChildList.add("Fairy Rings");
        return habitGroupingChildList;
    }

    private static ArrayList<String> createHabitOtherChildList() {
        ArrayList<String> habitOtherChildList = new ArrayList<String>();
        habitOtherChildList.add("Notes:"); //need text entry
        return habitOtherChildList;
    }

}
