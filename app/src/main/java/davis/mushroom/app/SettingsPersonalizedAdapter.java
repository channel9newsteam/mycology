package davis.mushroom.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by quazeenii on 4/28/14.
 */
public class SettingsPersonalizedAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<String> parentList;
    private ArrayList<ArrayList<String>> nestedParentList;
    private GlobalVariables appState;
    private LayoutInflater inflater;


    public SettingsPersonalizedAdapter(Context context, ArrayList<String> parentList,
                                              ArrayList<ArrayList<String>> nestedParentList,
                                              GlobalVariables appState) {
        this.context = context;
        this.parentList = parentList;
        this.nestedParentList = nestedParentList;
        this.appState = appState;
        inflater = LayoutInflater.from(context);

    }

    @Override
    public int getGroupCount() {
        return parentList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return nestedParentList.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int i, int i2) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int i, int i2) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean b, View view, ViewGroup viewGroup) {
        TextView tv = new TextView(context);
        tv.setText(parentList.get(groupPosition));
        tv.setPadding(50,10,10,10);
        tv.setTextSize(30);

        return tv;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean b, final View view, final ViewGroup viewGroup) {
        View v;
        if (view != null) {
            v = view;
        } else {
            v = inflater.inflate(R.layout.activity_advanced_child, viewGroup, false);
        }

        final TextView tv = (TextView) v.findViewById(R.id.childname);
        final CheckBox cb = (CheckBox) v.findViewById(R.id.check1);
        final EditText ev = (EditText) v.findViewById(R.id.input);


        tv.setText(nestedParentList.get(groupPosition).get(childPosition));
        tv.setPadding(5, 10, 10, 10);
        ev.setVisibility(View.INVISIBLE);
        cb.setChecked(getInputState(groupPosition, childPosition).equals("true"));
        cb.setVisibility(View.VISIBLE);
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb.isChecked()) {
                    setInput(groupPosition, childPosition, "true");
                } else {
                    setInput(groupPosition, childPosition, "false");
                }
                FungusDb fdb = new FungusDb(context);
                fdb.savePersonalizedSettingsToDb();
            }
        });

        //checks to see if they clicked the text
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!cb.isChecked()) {
                    cb.setChecked(true);
                    setInput(groupPosition, childPosition, "true");
                } else {
                    cb.setChecked(false);
                    setInput(groupPosition, childPosition, "false");
                }
                FungusDb fdb = new FungusDb(context);
                fdb.savePersonalizedSettingsToDb();
            }
        });
        return v;
    }

    private void setInput(int groupPosition, int childPosition, String value) {
        appState.personalizedSelectedInput.put(nestedParentList.get(groupPosition).get(childPosition), value);
    }

    private String getInputState(int groupPosition, int childPosition) {
        return appState.personalizedSelectedInput.get(nestedParentList.get(groupPosition).get(childPosition));
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

}