package davis.mushroom.app;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

public class MainActivity extends Activity {

    public final static String EXTRA_MESSAGE = "davis.Mushroom.MESSAGE";
    // Shared Preferences file.
    public static final String SHARED_PREFERENCES = "davis.mushroom.app.SHARED_PREFERENCES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FungusDb fdb = new FungusDb(getApplicationContext());
        fdb.repopulatePersonalizedSettingsFromDb();
    }

    //this is a test of the revision control system.

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.\

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void basicInput(View view)
    {
        Intent intent = new Intent(this, CatalogingFinds.class);
        startActivity(intent);
    }

    public void viewData(View view)
    {
        Intent intent = new Intent(this, SavedData.class);
        startActivity(intent);
    }


    public void getDichotomous(View view)
    {
        Intent intent = new Intent(this, DichotomousKeyActivity.class);
        startActivity(intent);
    }

    public void viewDictionary(View view) {
        Intent intent = new Intent(this, DictionaryActivity.class);
        startActivity(intent);
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.settings, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

}
