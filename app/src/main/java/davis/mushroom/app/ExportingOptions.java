package davis.mushroom.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphObject;
import com.facebook.widget.FacebookDialog;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by quazeenii on 5/3/14.
 */
public class ExportingOptions extends Activity {
    private static final String TAG = "ExportingFinds";
    private boolean canPresentShareDialog;
    private boolean canPresentShareDialogWithPhotos;
    private UiLifecycleHelper uiHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);

        // Can we present the share dialog for regular links?
        canPresentShareDialog = FacebookDialog.canPresentShareDialog
                (this, FacebookDialog.ShareDialogFeature.SHARE_DIALOG);
        // Can we present the share dialog for photos?
        canPresentShareDialogWithPhotos = FacebookDialog.canPresentShareDialog
                (this, FacebookDialog.ShareDialogFeature.PHOTOS);
    }


    public void postTweet() {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        String text = "No internet connection";

        // Check network status
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        // Create intent using ACTION_VIEW and a normal Twitter url if network available
        if(isConnected) {
            String tweetUrl =
                    String.format("https://twitter.com/intent/tweet?text=%s",
                            urlEncode("Look at what I found!"));
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

            // Narrow down to official Twitter app, if available:
            List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
            for (ResolveInfo info : matches) {
                if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                    intent.setPackage(info.activityInfo.packageName);
                }
            }
            startActivity(intent);
        } else {
            Toast.makeText(context, text, duration).show();
        }
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        }
        catch (UnsupportedEncodingException e) {
            Log.wtf("Unsupported Encoding Execution", "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    public void createCsvFile(String uniqueIdentifier) {
        FungusDb fungusDb = new FungusDb(getApplicationContext());
        ArrayList<ArrayList<String>> tuples = fungusDb.getFindsFromDb("*", "UniqueIdentifier = " + uniqueIdentifier, null);
        ArrayList<String> columnName = fungusDb.getColumnNames();

        File csvFile;
        BufferedWriter bw = null;
        String delimiter = ",";

        String csvFilePath = GlobalVariables.DIR + uniqueIdentifier + ".csv"; //need to insert the actual file path here (Please do this Irene!)
        try {
            csvFile = new File(csvFilePath);
            bw = new BufferedWriter(new FileWriter(csvFile));

            for (int i = 0; i < tuples.get(0).size(); i++) {
                bw.write(columnName.get(i));
                bw.write(delimiter);
                bw.write(tuples.get(0).get(i));
                bw.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    // Facebook requires the facebook application to be installed on the device.
    private interface GraphObjectWithId extends GraphObject {
        String getId();
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            //onSessionStateChange(session, state, exception);
        }
    };

    private FacebookDialog.Callback dialogCallback = new FacebookDialog.Callback() {
        @Override
        public void onError(FacebookDialog.PendingCall pendingCall,
                            Exception error, Bundle data) {
            Log.d("HelloFacebook", String.format("Error: %s", error.toString()));
        }

        @Override
        public void onComplete(FacebookDialog.PendingCall pendingCall,
                               Bundle data) {
            Log.d("HelloFacebook", "Success!");
        }
    };

    private boolean hasPublishPermission() {
        Session session = Session.getActiveSession();
        return session != null && session.getPermissions().contains("publish_actions");
    }

    private FacebookDialog.ShareDialogBuilder
    createShareDialogBuilderForLink() {
        final String DESCRIPTION = "The 'Hello Facebook' sample application " +
                "showcases simple Facebook integration";
        return new FacebookDialog.ShareDialogBuilder(this)
                .setName("Hello Facebook")
                .setDescription(DESCRIPTION)
                .setLink("http://developers.facebook.com/android");
    }

    public void postStatusUpdate() {
        Session session = Session.getActiveSession();

        if (canPresentShareDialog) {
            Log.i(TAG, "canPresentShareDialog");
            FacebookDialog shareDialog = createShareDialogBuilderForLink()
                    .build();
            uiHelper.trackPendingDialogCall(shareDialog.present());
        } else if (hasPublishPermission()) {
            Log.i(TAG, "hasPublishPermission()");
            if (session == null || !session.isOpened()) {
                Log.e(TAG, "Session is invalid");
                return;
            }

            final String message = getString(R.string.status_update, null,
                    (new Date().toString()));
            Request request = Request
                    .newStatusUpdateRequest(Session.getActiveSession(), message,
                            null, null, new Request.Callback() {
                                @Override
                                public void onCompleted(Response response) {
                                    showPublishResult(message,
                                            response.getGraphObject(),
                                            response.getError());
                                }
                            });
            request.executeAsync();
        }
    }

    private void showPublishResult(String message, GraphObject result,
                                   FacebookRequestError error) {
        String title = null;
        String alertMessage = null;
        if (error == null) {
            title = getString(R.string.success);
            String id = result.cast(GraphObjectWithId.class).getId();
            alertMessage = getString(R.string.successfully_posted_post, message,
                    id);
        } else {
            title = getString(R.string.error);
            alertMessage = error.getErrorMessage();
        }

        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(alertMessage)
                .setPositiveButton(R.string.ok, null)
                .show();
    }
}
