package davis.mushroom.app;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Zing
 */
public class DictionaryEntryActivity extends Activity {
    // Constants
    private static final int NAME_POS = 0;
    private static final int SCIENTIFIC_NAME_POS = 1;
    private static final int TYPE_POS = 2;
    private static final int OTHER_NAMES_POS = 3;
    private static final int KEY_FEATURE_START_POS = 4;
    private static final int KEY_FEATURE_END_POS = 12;
    private static final int OTHER_FEATURES_POS = 13;
    private static final int WHERE_POS = 14;
    private static final int EDIBILITY_POS = 15;
    private static final int NOTE_POS = 16;

    private static final String TAG = "DictionaryEntryActivity";
    private static final String BLANK = "\n";

    // Instance Variables
    private int dictionaryPos;
    private ArrayList<ArrayList<String>> queryResult;

    // Layout variables
    private TextView nameEntry, scientificNameEntry, otherNamesEntry, typeEntry;
    private TextView keyFeaturesEntry, otherFeaturesEntry;
    private TextView whereEntry, edibilityEntry, noteEntry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FungusDb fungusDb;
        Intent intent;
        String mushroomType;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionary_entry);

        nameEntry = (TextView) findViewById(R.id.entry_mushroom_name_entry);
        scientificNameEntry = (TextView)
                findViewById(R.id.entry_mushroom_scientific_name_entry);
        otherNamesEntry = (TextView)
                findViewById(R.id.entry_mushroom_other_names_entry);
        typeEntry = (TextView) findViewById(R.id.entry_mushroom_type_entry);
        keyFeaturesEntry = (TextView)
                findViewById(R.id.entry_mushroom_key_features_entry);
        otherFeaturesEntry = (TextView)
                findViewById(R.id.entry_mushroom_other_features_entry);
        whereEntry = (TextView) findViewById(R.id.entry_mushroom_where_entry);
        edibilityEntry = (TextView)
                findViewById(R.id.entry_mushroom_edibility_entry);
        noteEntry = (TextView) findViewById(R.id.entry_mushroom_note_entry);

        fungusDb = new FungusDb(getApplicationContext());

        Bundle extras = getIntent().getExtras();
        mushroomType = extras.getString(DictionaryExpandableListAdapter.MUSHROOM_TYPE);

        Log.e(TAG, "WHERE: Type = \'" + mushroomType + '\'');
        queryResult = fungusDb.getDbInfo("*", FungusDb.DICTIONARY_TABLE,
                "Type = \'" + mushroomType + '\'', null);

        dictionaryPos = Integer.valueOf(extras.getString(DictionaryExpandableListAdapter.DICTIONARY_INDEX));

        assignEntryValues();

        // Query to find the corresponding
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.\

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.settings, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

    private void assignEntryValues() {
        if ((queryResult != null && queryResult.size() > 0
                && queryResult.get(dictionaryPos) != null)) {
            // Name Entry
            if (queryResult.get(dictionaryPos).get(NAME_POS) != null) {
                nameEntry.setText(queryResult.get(dictionaryPos).get(NAME_POS)
                        + '\n');
            } else {
                nameEntry.setText(BLANK);
            }

            // Scientific Name Entry
            if (queryResult.get(dictionaryPos).get(SCIENTIFIC_NAME_POS) != null) {
                scientificNameEntry.setText(queryResult.get(dictionaryPos)
                        .get(SCIENTIFIC_NAME_POS) + '\n');
            } else {
                scientificNameEntry.setText(BLANK);
            }

            // Other Names Entry
            if (queryResult.get(dictionaryPos).get(OTHER_NAMES_POS) != null) {
                otherNamesEntry.setText(queryResult.get(dictionaryPos)
                        .get(OTHER_NAMES_POS) + '\n');
            } else {
                otherNamesEntry.setText(BLANK);
            }

            // Type Entry
            if (queryResult.get(dictionaryPos).get(TYPE_POS) != null) {
                typeEntry.setText(queryResult.get(dictionaryPos)
                        .get(TYPE_POS) + '\n');
            } else {
                typeEntry.setText(BLANK);
            }

            // Key Feature Entry(s)
            setKeyFeaturesTextField();

            // Other Features Entry
            if (queryResult.get(dictionaryPos)
                    .get(OTHER_FEATURES_POS) != null) {
                otherFeaturesEntry.setText(queryResult.get(dictionaryPos)
                        .get(OTHER_FEATURES_POS) + '\n');
            } else {
                otherFeaturesEntry.setText(BLANK);
            }

            // Where Entry
            if (queryResult.get(dictionaryPos).get(WHERE_POS) != null) {
                whereEntry.setText(queryResult.get(dictionaryPos)
                        .get(WHERE_POS) + '\n');
            } else {
                whereEntry.setText(BLANK);
            }

            // Edibility Entry
            if (queryResult.get(dictionaryPos).get(EDIBILITY_POS) != null) {
                edibilityEntry.setText(queryResult.get(dictionaryPos)
                        .get(EDIBILITY_POS) + '\n');
            } else {
                edibilityEntry.setText(BLANK);
            }

            // Note Entry
            if (queryResult.get(dictionaryPos).get(EDIBILITY_POS) != null) {
                noteEntry.setText(queryResult.get(dictionaryPos)
                        .get(NOTE_POS) + '\n');
            } else {
                noteEntry.setText(BLANK);
            }
        }
    }

    private void setKeyFeaturesTextField() {
        int i;

        if (queryResult.get(dictionaryPos).get(KEY_FEATURE_START_POS) != null) {
            StringBuilder field = new StringBuilder();
            for (i = KEY_FEATURE_START_POS; i < KEY_FEATURE_END_POS
                    && queryResult.get(dictionaryPos).get(i) != null; i++) {
                field.append('-');
                field.append(queryResult.get(dictionaryPos).get(i));
                field.append('\n');
            }
            keyFeaturesEntry.setText(field.toString());
        } else {
            keyFeaturesEntry.setText(BLANK);
        }
    }

    public void previousEntry(View view) {
        Log.i(TAG, "previousEntry()");
        if ((dictionaryPos - 1) >= 0) {
            dictionaryPos--;
        }

        assignEntryValues();
    }

    public void nextEntry(View view) {
        Log.i(TAG, "nextEntry()");
        if ((dictionaryPos + 1) < queryResult.size()) {
            dictionaryPos++;
        }

        assignEntryValues();
    }
}
