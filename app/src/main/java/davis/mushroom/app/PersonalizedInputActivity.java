package davis.mushroom.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import java.util.ArrayList;

/**
 * Created by quazeenii on 4/22/14.
 */
public class PersonalizedInputActivity extends FragmentActivity {
    public ExpandableListView exv;
    private GlobalVariables appState;

    ArrayList<String> parentList;
    ArrayList<ArrayList<String>> nestedParentList;
    ArrayList<ArrayList<ArrayList<String>>> childList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appState = ((GlobalVariables) getApplicationContext());
        setContentView(R.layout.activity_personal);
        exv = (ExpandableListView) findViewById(R.id.expandableListView);

        prepareNestedParentListAndChildList();
        exv.setAdapter(new AdvancedInputExpandableListAdapter(this, parentList,
                childList, nestedParentList, appState));
    }

    private void prepareNestedParentListAndChildList() {
        parentList =  AdvancedInputUtils.createParentList();
        nestedParentList = AdvancedInputUtils.createNestedParentList();
        childList = AdvancedInputUtils.createChildList();

        for (String characteristic : appState.personalizedSelectedInput.keySet()) {
            if (appState.personalizedSelectedInput.get(characteristic).equals("false")) {
                removeNestedChildList(characteristic);
                removeCharacteristicFromList(characteristic);
            }
        }
    }

    private void removeNestedChildList(String characteristic) {
        for (int i = 0; i < nestedParentList.size(); i++) {
            for (int j = 0; j < nestedParentList.get(i).size(); j++) {
                if (nestedParentList.get(i).get(j).equals(characteristic)) {
                    childList.get(i).remove(j);
                }
            }
        }
    }

    private void removeCharacteristicFromList(String characteristic) {
        for (ArrayList<String> nestedList : nestedParentList) {
            if (nestedList.contains(characteristic)) {
                nestedList.remove(characteristic);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        prepareNestedParentListAndChildList();
        exv.setAdapter(new AdvancedInputExpandableListAdapter(this, parentList,
                childList, nestedParentList, appState));
    }

    public void gotoPersonalSettings(View view) {
        Intent intent = new Intent(this, SettingsPersonalized.class);
        startActivity(intent);
    }

}

