package davis.mushroom.app;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

/**
 * Created by Garrett on 4/8/14.
 */
public class AdvancedInputActivity extends FragmentActivity {

    public ExpandableListView exv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GlobalVariables appState = ((GlobalVariables) getApplicationContext());
        setContentView(R.layout.activity_advanced_input);
        exv = (ExpandableListView) findViewById(R.id.expandableListView);
        exv.setAdapter(new AdvancedInputExpandableListAdapter(this, AdvancedInputUtils.createParentList(),
                AdvancedInputUtils.createChildList(), AdvancedInputUtils.createNestedParentList(), appState));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        for (int i = 0; i < exv.getExpandableListAdapter().getGroupCount(); i++) {
            exv.collapseGroup(i);
        }
    }

}
