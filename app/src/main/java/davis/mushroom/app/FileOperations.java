package davis.mushroom.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * Created by quazeenii on 5/1/14.
 */
public class FileOperations {

    public static void copyFiles(File source, File destination) throws IOException {
        if (!source.exists()) {
            return;
        }
        if (!destination.exists()) {
            destination.createNewFile();
        }
        if (source.isDirectory()) {
            String files[] = source.list();
            for (String f : files) {
                copyFiles(new File(source.getAbsolutePath() + File.separator + f), new File(destination, f));
                System.out.println("File names : " + source.getAbsolutePath() + f);
            }
        } else {
            FileChannel sourceChannel = new FileInputStream(source).getChannel();
            FileChannel destinationChannel = new FileOutputStream(destination).getChannel();
            if (destinationChannel != null && sourceChannel != null) {
                destinationChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
            }
            if (sourceChannel != null) {
                sourceChannel.close();
            }
            if (destinationChannel != null) {
                destinationChannel.close();
            }
        }
    }

    public static void deleteFiles(File deleted) {
        if (deleted.isDirectory()) {
            if (deleted.list().length == 0) {
                deleted.delete();
            } else {
                String files[] = deleted.list();
                for (String t : files) {
                    File fileDelete = new File(deleted, t);
                    deleteFiles(fileDelete);
                }

                if (deleted.list().length == 0) {
                    deleted.delete();
                }
            }
        } else {
            deleted.delete();
        }
    }
}
