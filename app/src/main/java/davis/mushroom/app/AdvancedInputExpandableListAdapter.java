package davis.mushroom.app;

import android.content.Context;
import android.graphics.Typeface;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Garrett on 4/8/14.
 */
public class AdvancedInputExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<String> parentList;
    private ArrayList<ArrayList<ArrayList<String>>> childList;
    private ArrayList<ArrayList<String>> nestedParentList;
    private GlobalVariables appState;

    public AdvancedInputExpandableListAdapter(Context context, ArrayList<String> parentList,
                                              ArrayList<ArrayList<ArrayList<String>>> childList,
                                              ArrayList<ArrayList<String>> nestedParentList,
                                              GlobalVariables appState) {
        this.context = context;
        this.parentList = parentList;
        this.childList = childList;
        this.nestedParentList = nestedParentList;
        this.appState = appState;
    }

    @Override
    public int getGroupCount() {
        return parentList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int i, int i2) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int i, int i2) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean b, View view, ViewGroup viewGroup) {
        TextView tv = new TextView(context);
        tv.setText(parentList.get(groupPosition));
        tv.setPadding(50,10,10,10);
        tv.setTextSize(30);

        return tv;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, final boolean b, final View view, final ViewGroup viewGroup) {
        CustExpListview SecondLevelexplv = new CustExpListview(context);
        SecondLevelexplv.setAdapter(new AdvancedInputSecondLevelExpandableListAdapter(context, nestedParentList.get(groupPosition), childList.get(groupPosition)));
        SecondLevelexplv.setPadding(40, 10, 10, 10);
        SecondLevelexplv.setSelector(android.R.color.transparent);

        return SecondLevelexplv;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }


    public class CustExpListview extends ExpandableListView {

       public CustExpListview(Context context) {
            super(context);
        }

       @Override
       protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE,
                    MeasureSpec.EXACTLY);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE,
                    MeasureSpec.EXACTLY);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }


    public class AdvancedInputSecondLevelExpandableListAdapter extends BaseExpandableListAdapter {

        private Context context;
        private ArrayList<String> parentList;
        private ArrayList<ArrayList<String>> childList;
        private LayoutInflater inflater;

        public AdvancedInputSecondLevelExpandableListAdapter(Context context, ArrayList<String> parentList,
                                                  ArrayList<ArrayList<String>> childList) {
            this.context = context;
            this.parentList = parentList;
            this.childList = childList;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getGroupCount() {
            return parentList.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return childList.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return groupPosition;
        }

        @Override
        public Object getChild(int i, int i2) {
            return null;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int i, int i2) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean b, View view, ViewGroup viewGroup) {
            TextView tv = new TextView(context);
            tv.setText(parentList.get(groupPosition));
            tv.setPadding(65, 10, 10, 10);
            tv.setTextSize(25);
            tv.setTypeface(Typeface.SANS_SERIF);

            return tv;
        }

        @Override
        public View getChildView(final int groupPosition, final int childPosition, boolean b, View view, ViewGroup viewGroup) {
            View v;
            if (view != null) {
                v = view;
            } else {
                v = inflater.inflate(R.layout.activity_advanced_child, viewGroup, false);
            }

            final TextView tv = (TextView) v.findViewById(R.id.childname);
            final CheckBox cb = (CheckBox) v.findViewById(R.id.check1);
            final EditText input = (EditText) v.findViewById(R.id.input);

            tv.setText(childList.get(groupPosition).get(childPosition));
            tv.setPadding(5, 10, 10, 10);

            if (tv.getText().toString().endsWith(":")) { // needs editText
                cb.setVisibility(View.INVISIBLE);
                input.setVisibility(View.VISIBLE);
                input.setText(getInputState(groupPosition, childPosition));
                input.setOnKeyListener(new EditText.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        setInput(appState.selectedInput, groupPosition, childPosition, input.getText().toString());
                        return false;
                    }
                });

                input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(!hasFocus) {
                            setInput(appState.selectedInput, groupPosition, childPosition, input.getText().toString());
                        }
                    }
                });

            } else { // needs checkbox
                cb.setChecked(getInputState(groupPosition, childPosition).equals("true"));
                cb.setVisibility(View.VISIBLE);
                input.setVisibility(View.INVISIBLE);
                cb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (cb.isChecked()) {
                            setInput(appState.selectedInput, groupPosition, childPosition, "true");
                        } else {
                            setInput(appState.selectedInput, groupPosition, childPosition, "false");
                        }
                    }
                });

                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!cb.isChecked()) {
                            cb.setChecked(true);
                            setInput(appState.selectedInput, groupPosition, childPosition, "true");
                        } else {
                            cb.setChecked(false);
                            setInput(appState.selectedInput, groupPosition, childPosition, "false");
                        }
                    }
                });
            }

            return v;
        }

        private void setInput(HashMap<String, HashMap<String, String>> selectedInput,
                              int groupPosition, int childPosition, String value) {
            appState.selectedInput.get(parentList.get(groupPosition)).put(childList.get(groupPosition).get(childPosition), value);
        }

        private String getInputState(int groupPosition, int childPosition) {
            return appState.selectedInput.get(parentList.get(groupPosition)).get(childList.get(groupPosition).get(childPosition));
        }

        @Override
        public boolean isChildSelectable(int i, int i2) {
            return true;
        }

    }

}
