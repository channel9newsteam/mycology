package davis.mushroom.app;

import android.app.DialogFragment;
import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TabHost;
import android.widget.TextView;

/**
 * Created by quazeenii on 4/22/14.
 */
public class CatalogingFinds extends TabActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbing);
        TabHost tabHost=getTabHost();
        TextView basic = new TextView(this);
        TextView personalized = new TextView(this);
        TextView advanced = new TextView(this);

        basic.setTextSize(14);
        basic.setGravity(Gravity.CENTER);
        basic.setTypeface(Typeface.create("sans", Typeface.BOLD));
        personalized.setTextSize(14);
        personalized.setGravity(Gravity.CENTER);
        personalized.setTypeface(Typeface.create("sans", Typeface.BOLD));
        advanced.setTextSize(14);
        advanced.setGravity(Gravity.CENTER);
        advanced.setTypeface(Typeface.create("sans", Typeface.BOLD));
        getActionBar().setDisplayHomeAsUpEnabled(true);


        Intent in1 = new Intent(this, BasicInputActivity.class);
        TabHost.TabSpec spec1 = tabHost.newTabSpec("Basic");
        basic.setText("Basic");
        spec1.setIndicator(basic);
        spec1.setContent(in1);

        Intent in2 = new Intent(this, PersonalizedInputActivity.class);
        TabHost.TabSpec spec2=tabHost.newTabSpec("Personalized");
        personalized.setText("Personalized");
        spec2.setIndicator(personalized);
        spec2.setContent(in2);

        Intent in3 = new Intent(this, AdvancedInputActivity.class);
        TabHost.TabSpec spec3=tabHost.newTabSpec("Advanced");
        advanced.setText("Advanced");
        spec3.setIndicator(advanced);
        spec3.setContent(in3);

        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);
        tabHost.setCurrentTab(0);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.\

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.settings, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

}
