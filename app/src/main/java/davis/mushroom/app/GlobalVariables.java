package davis.mushroom.app;

import android.app.Application;
import android.os.Environment;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Garrett on 4/27/14.
 */
public class GlobalVariables extends Application {
    public static final String DIR = Environment.getExternalStorageDirectory() + File.separator + "Fungus" + File.separator;
    public static final String DIR_HIDDEN = DIR + ".hidden" + File.separator;
    public static final String DIR_TEMP = DIR_HIDDEN + "_temp" + File.separator;
    public static final String DIR_EXPORT = DIR + "Export" + File.separator;

    public static String currentDateTime;
    public static File mainFolder;
    public static File hiddenFolder;
    public static File tempFolder;
    public static File savedFolder;
    public static File savedMedia;
    public static File exportFolder;

    public HashMap<String, HashMap<String, String>> selectedInput;
    public HashMap<String, String> personalizedSelectedInput;
    private ArrayList<ArrayList<String>> nestedParentList;
    private ArrayList<ArrayList<ArrayList<String>>> childList;

    public static String mushroomType;
    public static boolean setType;
    public static ArrayList<String> checkedIds;
    public static boolean noReset;

    public static boolean showClear;

    private final String emptyString = "";

    public GlobalVariables() {
        mainFolder = new File(DIR);
        hiddenFolder = new File(DIR_HIDDEN);
        tempFolder = new File(DIR_TEMP);
        exportFolder = new File(DIR_EXPORT);

        mainFolder.mkdirs();
        hiddenFolder.mkdirs();
        tempFolder.mkdirs();
        exportFolder.mkdirs();
        savedFolder = null;
        savedMedia = null;
        setType = false;
        showClear = true;
        noReset = true;

        mushroomType = new String();
        currentDateTime = new String();

        nestedParentList = AdvancedInputUtils.createNestedParentList();
        childList = AdvancedInputUtils.createChildList();
        selectedInput = new HashMap<String, HashMap<String, String>>();
        initializeSelectedInput();

        personalizedSelectedInput = new HashMap<String, String>();
        initializePersonalizedSelectedInput();

        checkedIds = new ArrayList<String>();
    }

    public void initializeSelectedInput() {
        initializedAdvancedInputStorage();
        initializedBasicInputStorage();
    }

    private void initializedAdvancedInputStorage() {
        for (int i = 0; i < nestedParentList.size(); i++) {
            for (int j = 0; j < nestedParentList.get(i).size(); j++) {
                selectedInput.put(nestedParentList.get(i).get(j), new HashMap<String, String>());
                insertChildListInputMappings(selectedInput.get(nestedParentList.get(i).get(j)), i, j);
            }
        }
    }

    private void insertChildListInputMappings(HashMap<String, String> nestedChildListItems, int groupPosition, int childPosition) {
        String falseString = "false";
        for (String childName : childList.get(groupPosition).get(childPosition)) {
            if (childName.endsWith(":")) {
                nestedChildListItems.put(childName, emptyString);
            } else {
                nestedChildListItems.put(childName, falseString);
            }
        }
    }

    private void initializedBasicInputStorage() {
        String basicInputId = "basicInput";
        selectedInput.put(basicInputId, new HashMap<String, String>());
        HashMap<String, String> basicInput = selectedInput.get(basicInputId);
        basicInput.put("UniqueIdentifier", emptyString);
        basicInput.put("Date", emptyString);
        basicInput.put("Type", emptyString);
        basicInput.put("GPS", emptyString);
        basicInput.put("Media", emptyString);
        basicInput.put("Notes", emptyString);
    }

    public void initializePersonalizedSelectedInput() {
        String trueString = "true";
        for (ArrayList<String> parentList : nestedParentList) {
            for (String childName : parentList) {
                personalizedSelectedInput.put(childName, trueString);
            }
        }
    }

    public static void unbindDrawables(View view) {
        System.out.println("unbinding drawables");
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }
}
