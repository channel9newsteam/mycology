package davis.mushroom.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Garrett on 3/8/14.
 */

public class AboutDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("About");
        builder.setMessage("Fungus Finder is an application for mycologists to log finds for " +
                "amateur or professional use.\n\nDisclaimer: This application contains fungus " +
                "identification tools for categorizing fungus. However, this identification " +
                "should not be used as a basis for safe consumption of fungus.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        // Create the AlertDialog object and return it
        return builder.create();
    }

}
