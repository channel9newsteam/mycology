package davis.mushroom.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 * Created by quazeenii on 5/1/14.
 */
public class ListedFindsAdapter extends ArrayAdapter {
    private Context context;
    public List<ListedChildren> l;

    public ListedFindsAdapter(Context context, List<ListedChildren> items) {
        super(context, android.R.layout.simple_list_item_1, items);
        this.context = context;
        this.l = items;
    }

    public View getView(int position, View v, ViewGroup parent) {
        View view;
        final ListedChildren lc = l.get(position);
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (v == null) {
            view = mInflater.inflate(R.layout.activity_view_listed_children, parent, false);
        } else {
            view = v;
        }

        final CheckBox cb = (CheckBox) view.findViewById(R.id.checkbox_saved_data);
        RelativeLayout rl = (RelativeLayout) view.findViewById(R.id.view_data_listed);
        TextView date = (TextView) view.findViewById(R.id.date_info);
        TextView type = (TextView) view.findViewById(R.id.type_info);
        TextView location = (TextView) view.findViewById(R.id.location_info);
        ImageView thumbnail = (ImageView) view.findViewById(R.id.thumbnail_mini);
        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editFind(lc.getId());
            }
        });
        cb.setChecked(GlobalVariables.checkedIds.contains(lc.getId()));
        cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cb.isChecked()) {
                    GlobalVariables.checkedIds.add(lc.getId());
                } else {
                    GlobalVariables.checkedIds.remove(lc.getId());
                }
            }
        });

        date.setText(lc.getDate());
        type.setText(lc.getType());
        location.setText(lc.getLocation());
        Bitmap scaled = getFirstImage(lc.getMediaPath());

        if (scaled != null) {
            // Need to scale photo to deal with the massive picture.
            System.out.println(scaled.getRowBytes() * scaled.getHeight());
            thumbnail.setImageBitmap(scaled);
        }

        return view;
    }

    private Bitmap getFirstImage(String path) {
        final int REQ_HEIGHT = 256;
        final int REQ_WIDTH = 256;
        File media = new File(path);
        File[] files = media.listFiles();

        if (files == null || files.length == 0) {
            return null;
        } else {
            if (files[0].getAbsolutePath().contains("PIC")) {
                return MediaFunctions.getPhotoBitmapBounded(files[0], REQ_WIDTH, REQ_HEIGHT);
                //return MediaFunctions.getPhotoBitmap(files[0]);

            } else {
                return MediaFunctions.getVideoBitmap(files[0]);
            }
        }
    }

    private void editFind(String uniqueIdentifier) {
        HashMap<String, HashMap<String, String>> selectedInput = getSelectedInput();
        ArrayList<String> tupleToPopulate = getTupleToPopulate(uniqueIdentifier);

        resetSelectedInput();
        populateSelectedInputFromUniqueIdentifier(selectedInput, tupleToPopulate);
        GlobalVariables.currentDateTime = uniqueIdentifier;
        GlobalVariables.savedMedia = new File(selectedInput.get("basicInput").get("Media"));
        GlobalVariables.savedFolder = GlobalVariables.savedMedia.getParentFile();
        GlobalVariables.savedMedia.mkdirs();
        GlobalVariables.mushroomType = selectedInput.get("basicInput").get("Type");
        if (!GlobalVariables.mushroomType.equals("")) {
           GlobalVariables.setType = true;
        }
        GlobalVariables.showClear = false;
        GlobalVariables.noReset = false;

        Intent intent = new Intent(context, CatalogingFinds.class);
        ((Activity) context).startActivityForResult(intent, 1);
    }

        private HashMap<String, HashMap<String, String>> getSelectedInput() {
        return ((GlobalVariables) context.getApplicationContext()).selectedInput;
    }

    private ArrayList<String> getTupleToPopulate(String uniqueIdentifier) {
        FungusDb fungusDb = new FungusDb(context);
        return fungusDb.getFindsFromDb("*", "UniqueIdentifier = '" + uniqueIdentifier + "'", null).get(0);
    }

    private void resetSelectedInput() {
        ((GlobalVariables) context.getApplicationContext()).initializeSelectedInput();
    }

    private void populateSelectedInputFromUniqueIdentifier(HashMap<String, HashMap<String, String>> selectedInput, ArrayList<String> tupleToPopulate) {
        populateBasicInput(selectedInput, tupleToPopulate);
        populateAdvancedInput(selectedInput, tupleToPopulate);
    }

    private void populateBasicInput(HashMap<String, HashMap<String, String>> selectedInput, ArrayList<String> tupleToPopulate) {
        selectedInput.get("basicInput").put("UniqueIdentifier", tupleToPopulate.get(0));
        selectedInput.get("basicInput").put("Date", tupleToPopulate.get(1));
        selectedInput.get("basicInput").put("Type", tupleToPopulate.get(2));
        selectedInput.get("basicInput").put("GPS", tupleToPopulate.get(3));
        selectedInput.get("basicInput").put("Media", tupleToPopulate.get(4));
        selectedInput.get("basicInput").put("Notes", tupleToPopulate.get(5));
    }

    private void populateAdvancedInput(HashMap<String, HashMap<String, String>> selectedInput, ArrayList<String> tupleToPopulate) {
        int columnPosition = selectedInput.get("basicInput").size();
        ArrayList<ArrayList<String>> nestedParentList = AdvancedInputUtils.createNestedParentList();
        ArrayList<ArrayList<ArrayList<String>>> childList = AdvancedInputUtils.createChildList();
        for (int i = 0; i < nestedParentList.size(); i++) {
            for (int j = 0; j < nestedParentList.get(i).size(); j++) {
                columnPosition = populatedChildInput(selectedInput.get(nestedParentList.get(i).get(j)), i, j, childList, tupleToPopulate, columnPosition);
            }
        }
    }

    private int populatedChildInput(HashMap<String, String> childInputs, int groupPosition, int childPosition, ArrayList<ArrayList<ArrayList<String>>> childList, ArrayList<String> tupleToPopulate, int columnPosition) {
        for (String childName : childList.get(groupPosition).get(childPosition)) {
            childInputs.put(childName, tupleToPopulate.get(columnPosition));
            columnPosition++;
        }
        return columnPosition;
    }

}