package davis.mushroom.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by quazeenii on 5/25/14.
 */
public class RemoteFindsAdapter extends ArrayAdapter {
    private Context context;
    public ArrayList<HashMap<String, String>> remoteFinds;

    public RemoteFindsAdapter(Context context, ArrayList<HashMap<String, String>> items) {
        super(context, android.R.layout.simple_list_item_1, items);
        this.context = context;
        this.remoteFinds = items;
    }

    public View getView(int position, View v, ViewGroup parent) {
        //Bitmap image;
        View view;
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //RelativeLayout relativelayout;
        TextView date, type, location;
        //ImageView thumbnail;

        if (v == null) {
            view = mInflater.inflate(R.layout.activity_view_remote_listed_children, parent, false);
        } else {
            view = v;
        }

        //relativelayout = (RelativeLayout) view.findViewById(R.id.view_data_listed_remote);
        if (view != null) {
            date = (TextView) view.findViewById(R.id.date_info_remote);
            type = (TextView) view.findViewById(R.id.type_info_remote);
            location = (TextView) view.findViewById(R.id.location_info_remote);
            //thumbnail = (ImageView) view.findViewById(R.id.thumbnail_mini_remote);

            date.setText(remoteFinds.get(position).get(FungusDb.DATE));
            type.setText(remoteFinds.get(position).get(FungusDb.TYPE));
            location.setText(remoteFinds.get(position).get(FungusDb.GPS));

            // Convert the string bitmap into a bitmap
            //image = getTupleBitmap(position);
            //if (image != null) {
            // Need to scale photo to deal with the massive picture.
            //thumbnail.setImageBitmap(image);
            //}
        }

        return view;
    }

    /*
    // Function to return a tuple's bitmap.
    private Bitmap getTupleBitmap(int pos) {
        String image;

        if (remoteFinds == null || pos < 0 ||
                pos >= remoteFinds.size()) {
            return null;
        }

        image = remoteFinds.get(pos).get(FungusDb.MEDIA);

        if (image != null) {
            byte[] byteArray;
            Bitmap bm;

            byteArray = image.getBytes();
            bm = MediaFunctions.decodeByteArrayScaled(byteArray, 64, 64);
            //bm = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

            return bm;
        }

        return null;
    }
    */
}
