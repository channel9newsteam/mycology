package davis.mushroom.app;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by quazeenii on 5/13/14.
 */
public class ViewRemoteData extends Activity {
    // Tags for retrieving a specific query
    public static final String TAG_QUERY_NAME = "QueryName";
    public static final String TAG_QUERY = "Query";
    private static final int QUERY_NAME_POS = 0;
    private static final int QUERY_POS = 1;

    private static final String TAG = "ViewRemoteData";

    // Instance Variables
    // Remote Database
    private EditText searchCriteria;
    private Spinner sortBy;
    private ProgressDialog pDialog;
    private ArrayList<HashMap<String, String>> remoteFindsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_remote);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        Button browse = (Button) findViewById(R.id.browse_finds);

        browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getAllFindsFromRemoteDB();
            }
        });
    }
/*
    private void readInFinds() {
        for ()
        listAdapter = new RemoteFindsAdapter(this, remoteFindsList);
        ListView lv = (ListView) findViewById(R.id.list);
        lv.setAdapter(listAdapter);
    }
*/
    @Override
    protected void onResume() {
        super.onResume();
        // Call the 'activateApp' method to log an app event for use in analytics and advertising reporting.  Do so in
        // the onResume methods of the primary Activities that an app may be launched into.
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.settings, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                // Auto-generated method stub
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_SHORT;
                CharSequence text;

                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

    // Remote finds
    private void getAllFindsFromRemoteDB() {
        Context context;

        context = getApplicationContext();

        // Check for network connection, if there is one then initiate it.
        if (context != null && ViewDataActivity.isNetworkConnected(context)) {
            // Finds will be in the remoteFindsList
            remoteFindsList = new ArrayList<HashMap<String, String>>();

            new LoadAllFinds().execute();

            // Finds are stored into remoteFindsList.
        } else if (context != null) {
            Toast toast = Toast.makeText(context,
                    ViewDataActivity.ERR_NO_NETWORK, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    // This code gets the search criteria and searches based on that, it still
    // needs code to change the type of query.
    public void getFindFromRemoteDB(View view) {
        final int QUERY_SIZE = 2;
        String criteria = null;
        String queryName = null;
        String[] queryArray;

        if (sortBy.getSelectedItem() != null) {
            queryName = sortBy.getSelectedItem().toString();
        }

        if (searchCriteria.getText() != null) {
            criteria = searchCriteria.getText().toString();
        }

        queryArray = new String[QUERY_SIZE];
        remoteFindsList = new ArrayList<HashMap<String, String>>();

        queryArray[QUERY_NAME_POS] = queryName;
        queryArray[QUERY_POS] = '\'' + criteria + '\'';

        GetFungusQuery fungusQuery = new GetFungusQuery(queryArray);
        fungusQuery.execute();
    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadAllFinds extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewRemoteData.this);
            pDialog.setMessage("Loading Finds. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * getting All products from url
         */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = JSONParser
                    .makeHttpRequest(FungusDb.URL_GET_ALL_FINDS,
                    JSONParser.GET, params);

            // Check your log cat for JSON reponse
            if (json == null) {
                Log.e(TAG, "JSON request failed");
                return null;
            }
            Log.d("All Products: ", json.toString());

            getInformationFromJSONArray(json);

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // Do any updates required.
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    /*
                     ListAdapter adapter = new SimpleAdapter(
                            AllProductsActivity.this, productsList,
                            R.layout.list_item, new String[] { TAG_PID,
                            TAG_NAME},
                            new int[] { R.id.pid, R.id.name });
                    // updating listview
                    setListAdapter(adapter);
                    */
                }
            });

            readInRemoteFinds();
        }
    }

    class GetFungusQuery extends AsyncTask<String, String, String> {
        private String[] query;

        public GetFungusQuery(final String[] query) {
            this.query = query;
        }

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ViewRemoteData.this);
            pDialog.setMessage("Loading Finds. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * getting All products from url
         */
        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair(TAG_QUERY_NAME,
                    query[QUERY_NAME_POS]));
            params.add(new BasicNameValuePair(TAG_QUERY, query[QUERY_POS]));

            // getting JSON string from URL
            JSONObject json = JSONParser.makeHttpRequest(FungusDb.URL_GET_FIND,
                    JSONParser.GET, params);

            // Check your log cat for JSON reponse
            if (json == null) {
                Log.e(TAG, "JSON request failed");
                return null;
            }
            Log.d("All Products: ", json.toString());

            getInformationFromJSONArray(json);

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        // Modify
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pDialog.dismiss();
            // Do any updates required.
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    /*
                     ListAdapter adapter = new SimpleAdapter(
                            AllProductsActivity.this, productsList,
                            R.layout.list_item, new String[] { TAG_PID,
                            TAG_NAME},
                            new int[] { R.id.pid, R.id.name });
                    // updating listview
                    setListAdapter(adapter);
                    */
                }
            });

            readInRemoteFinds();
        }
    }

    private void getInformationFromJSONArray(JSONObject json) {
        try {
            // Checking for SUCCESS TAG
            int success = json.getInt(FungusDb.SUCCESS);

            if (success == 1) {
                JSONArray remoteFinds;
                // products found
                // Getting Array of Products
                remoteFinds = json.getJSONArray(FungusDb.FUNGUS_TABLE);

                // looping through All Products
                for (int i = 0; i < remoteFinds.length(); i++) {
                    JSONObject c = remoteFinds.getJSONObject(i);

                    // Storing each json item in variable
                    String id = c.getString(FungusDb.UNIQUE_IDENTIFIER);
                    String date = c.getString(FungusDb.DATE);
                    String type = c.getString(FungusDb.TYPE);
                    String gps = c.getString(FungusDb.GPS);
                    String media = c.getString(FungusDb.MEDIA);
                    String notes = c.getString(FungusDb.NOTES);

                    // creating new HashMap
                    HashMap<String, String> map = new HashMap<String, String>();

                    // adding each child node to HashMap key => value
                    map.put(FungusDb.UNIQUE_IDENTIFIER, id);
                    map.put(FungusDb.DATE, date);
                    map.put(FungusDb.TYPE, type);
                    map.put(FungusDb.GPS, gps);
                    map.put(FungusDb.MEDIA, media);
                    map.put(FungusDb.NOTES, notes);

                    // adding HashList to ArrayList
                    remoteFindsList.add(map);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void readInRemoteFinds() {
        final TextView tv = (TextView) findViewById(R.id.no_finds_message);

        if (remoteFindsList.isEmpty()) {
            tv.setText("There are no finds in the database.");
        } else {
            RemoteFindsAdapter listAdapter;
            ListView listView;

            tv.setVisibility(View.INVISIBLE);
            listView = (ListView) findViewById(R.id.list);
            listAdapter = new RemoteFindsAdapter(this, remoteFindsList);
            listView.setAdapter(listAdapter);
        }
    }
}
