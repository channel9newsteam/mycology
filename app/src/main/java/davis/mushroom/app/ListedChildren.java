package davis.mushroom.app;

/**
 * Created by quazeenii on 5/1/14.
 */
public class ListedChildren {
    public FungusDb db;
    private String id;

    public ListedChildren(String id, FungusDb db) {
        this.id = id;
        this.db = db;
    }

    public String getId () {
        return id;
    }

    public String getDate () {
        String date = db.getFindsFromDb("Date", "UniqueIdentifier = '" + id + "'", "UniqueIdentifier").get(0).get(0);
        return date;
    }

    public String getType () {
        String type = db.getFindsFromDb("Type", "UniqueIdentifier = '" + id + "'", "UniqueIdentifier").get(0).get(0);
        if (type.equals("")) {
            type = "<unknown>";
        }
        return type;
    }

    public String getLocation () {
        String location = db.getFindsFromDb("GPS", "UniqueIdentifier = '" + id + "'", "UniqueIdentifier").get(0).get(0);
        if (location.equals("")) {
            location = "<unknown>";
        }
        return location;
    }

    public String getMediaPath () {
        String path = db.getFindsFromDb("Media", "UniqueIdentifier = '" + id + "'", "UniqueIdentifier").get(0).get(0);
        return path;
    }
}
