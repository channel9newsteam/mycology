package davis.mushroom.app;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by yesjeongson on 6/1/14.
 */

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        String str = ((Button) getActivity().findViewById(R.id.basic_input_edit_date)).getText().toString();
        String[] tokens = str.split(" ");

        int month = 0;
        switch(Months.valueOf(tokens[0])) {
            case January:
                month = 0;
                break;
            case February:
                month = 1;
                break;
            case March:
                month = 2;
                break;
            case April:
                month = 3;
                break;
            case May:
                month = 4;
                break;
            case June:
                month = 5;
                break;
            case July:
                month = 6;
                break;
            case August:
                month = 7;
                break;
            case September:
                month = 8;
                break;
            case October:
                month = 9;
                break;
            case November:
                month = 10;
                break;
            case December:
                month = 11;
                break;

        }
        int day = Integer.parseInt(tokens[1]);
        int year = Integer.parseInt(tokens[2]);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);
        ((Button) getActivity().findViewById(R.id.basic_input_edit_date)).setText(new SimpleDateFormat("MMMM dd yyyy").format(cal.getTime()));
    }

    public enum Months{
        January, February, March, April, May, June, July, August, September, October, November, December;
    }
}