package davis.mushroom.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by quazeenii on 4/19/14.
 */
public class MediaFunctions extends BasicInputActivity {
    protected static ImageView[] images;
    protected static File mediaFile;
    protected static int count;

    public static void readIn(Activity activity, final File directory) {
        count = 0;
        if (!directory.exists() || directory.length() == 0) {
            return;
        }
        for (final File f : directory.listFiles()) {
            if (f.isDirectory()) {
                readIn(activity, f);
            } else {
                String path = f.getAbsolutePath();
                try {
                    File input = new File(path);
                    MediaFunctions.createThumbnail(activity, input);
                } catch (Exception ex) {
                }
            }

        }
    }

    public static File createImageFile(File directory) throws IOException {
        //creates filename for pictures
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String image = "PIC_" + time;
        File picture = new File(directory, image+".jpg");
        return picture;
    }

    public static File createVideoFile(File directory) throws IOException {
        //creates filename for videos
        String time = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String image = "VID_" + time;
        File picture = new File(directory, image+".mp4");
        return picture;
    }

    public static void createThumbnail (final Activity activity, final File path) {
        if (count < 8) {
            Bitmap media;
            if (path.getAbsolutePath().contains("PIC")) {
                media = getPhotoBitmap(path);
            } else {
                media = getVideoBitmap(path);
            }
            images[count].setImageBitmap(media);
            images[count].setTag(path.getAbsolutePath());
            images[count].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (v.getTag() != "") {
                        setPreview(activity, v);
                    }
                }

            });
        }
        count++;
    }

    private static void setPreview(final Activity activity, View v) {
        final Dialog preview = new Dialog(activity);
        preview.requestWindowFeature(Window.FEATURE_NO_TITLE);
        preview.setContentView(R.layout.media_preview);
        final File file = new File(v.getTag().toString());
        if (file.getAbsolutePath().contains("PIC")) {
            ImageView image = (ImageView) preview.findViewById(R.id.preview_image);
            image.setScaleType(ImageView.ScaleType.FIT_XY);
            BitmapFactory.Options options = new BitmapFactory.Options();
            try {
                Matrix matrix = new Matrix();
                matrix.postRotate(getDegrees(file));
                options.inSampleSize = calculateInSampleSize(options, image.getHeight(), image.getWidth());
                Bitmap enlarged = decodeFile(file, image.getMaxHeight(), image.getMaxWidth());
                Bitmap rotated = Bitmap.createBitmap(enlarged, 0, 0, enlarged.getWidth(),
                        enlarged.getHeight(), matrix, true);
                image.setImageBitmap(rotated);
            } catch (Exception ex) {
            }
        } else {
            final VideoView video = (VideoView) preview.findViewById(R.id.preview_video);
            video.setVideoPath(v.getTag().toString());
            video.start();
            video.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (video.isPlaying()) {
                        video.pause();
                    } else {
                        video.start();
                    }
                    return false;
                }
            });
        }
        Button delete = (Button) preview.findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog deleteMenu = new Dialog(activity);
                deleteMenu.setContentView(R.layout.options_yes_no);
                deleteMenu.setTitle("Delete");
                TextView tv = (TextView) deleteMenu.findViewById(R.id.message);
                tv.setText("Are you sure?");
                Button yes = (Button) deleteMenu.findViewById(R.id.button_yes);
                yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteMenu.cancel();
                        preview.cancel();
                        resetThumbnails();
                        File parent = file.getParentFile();
                        FileOperations.deleteFiles(file);
                        readIn(activity, parent);
                    }
                });
                Button no = (Button) deleteMenu.findViewById(R.id.button_no);
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        deleteMenu.cancel();
                        GlobalVariables.unbindDrawables(deleteMenu.findViewById(R.id.media_preview));
                    }
                });
                deleteMenu.show();
            }
        });
        Button cancel = (Button) preview.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                preview.cancel();
            }

        });

        preview.show();
    }

    public static Bitmap getVideoBitmap(File file) {
        int SIZE = 256;
        int degrees = getDegrees(file);
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        Bitmap imageBitmap = ThumbnailUtils.createVideoThumbnail(file.getAbsolutePath(), MediaStore.Video.Thumbnails.MINI_KIND);
        imageBitmap = Bitmap.createScaledBitmap(imageBitmap, SIZE, SIZE, false);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        Bitmap rotated = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(),
                imageBitmap.getHeight(), matrix, true);
        return rotated;
    }

    public static Bitmap getPhotoBitmap(File file) {
        final int SIZE = 256;
        int degrees = getDegrees(file);
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        try {
            Bitmap imageBitmap = decodeFile(file, SIZE, SIZE);
            //Bitmap imageBitmap = getPhotoBitmapBounded(file, SIZE, SIZE);
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap, SIZE, SIZE, true);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            Bitmap rotated = Bitmap.createBitmap(imageBitmap, 0, 0, imageBitmap.getWidth(),
                    imageBitmap.getHeight(), matrix, true);
            return rotated;
        } catch (Exception ex) {
        }
        return null;
    }

    public static Bitmap decodeFile(File f, int width, int height) {
        int scale = 1;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, options);
            while (options.outWidth/scale >= width && options.outHeight/scale >= height) {
                scale *=2;
            }

            BitmapFactory.Options newOptions = new BitmapFactory.Options();
            newOptions.inSampleSize=scale;
            Bitmap image = BitmapFactory.decodeStream(new FileInputStream(f), null, newOptions);
            System.out.println("Bitmap Image " + image.getByteCount());
            return image;
        } catch(FileNotFoundException ex) {
        }
        return null;
    }

    public static Bitmap getPhotoBitmapBounded(File file, int reqWidth, int reqHeight) {
        try {
            int degrees = getDegrees(file);
            Bitmap imageBitmap, rotated;
            BitmapFactory.Options options = new BitmapFactory.Options();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Matrix matrix = new Matrix();

            matrix.postRotate(degrees);
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Reinitialize the stream, because we used it.
            // This time read in the image with the sample size.
            options.inJustDecodeBounds = false;
            imageBitmap = BitmapFactory.decodeStream(new FileInputStream(file), null,
                    options);

            imageBitmap.compress(Bitmap.CompressFormat.JPEG, 50, baos);
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap,
                    options.outWidth/options.inSampleSize,
                    options.outHeight/options.inSampleSize, false);
            rotated = Bitmap.createBitmap(imageBitmap, 0, 0,
                    imageBitmap.getWidth(), imageBitmap.getHeight(), matrix,
                    true);

            return rotated;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static Bitmap decodeBitmapResourceBounded(Resources resource,
                                                      int id, int reqWidth,
                                                      int reqHeight) {
        // Add orientation
        Bitmap imageBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resource, id, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // This time read in the image with the sample size.
        options.inJustDecodeBounds = false;
        imageBitmap = BitmapFactory.decodeResource(resource, id, options);

        imageBitmap = Bitmap.createScaledBitmap(imageBitmap, options.outWidth/options.inSampleSize,
                options.outHeight/options.inSampleSize, false);

        return imageBitmap;
    }

    public static Bitmap decodeByteArrayScaled(final byte[] byteArray,
                                               final int reqWidth,
                                               final int reqHeight) {
        Bitmap imageBitmap;
        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        options.inJustDecodeBounds = false;
        imageBitmap = BitmapFactory.decodeByteArray(byteArray, 0,
                byteArray.length, options);

        if (imageBitmap != null) {
            imageBitmap = Bitmap.createScaledBitmap(imageBitmap,
                    options.outWidth / options.inSampleSize,
                    options.outHeight / options.inSampleSize, false);
        }

        return imageBitmap;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        int height = options.outHeight;
        int width = options.outWidth;

        int inSampleSize = 1;

        while (height > reqHeight || width > reqWidth) {
            inSampleSize = inSampleSize * 2;

            height = height / inSampleSize;
            width = width / inSampleSize;
        }

        return inSampleSize;
    }

    public static int getDegrees(File file) {
        int degrees = 0;
        try {
            ExifInterface exif = new ExifInterface(file.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            switch (orientation) {
                case 3:
                    degrees = 180;
                    break;
                case 6:
                    degrees = 90;
                    break;
                case 8:
                    degrees = 270;
                    break;
                default:
                    degrees = 0;
                    break;
            }
        } catch(Exception ex) {
        }
        return degrees;
    }

    private static void resetThumbnails() {
        for (ImageView i : images) {
            i.setImageBitmap(null);
            i.setTag("");
            i.setImageResource(R.drawable.camera_logo);
            count = 0;
        }
    }
}
