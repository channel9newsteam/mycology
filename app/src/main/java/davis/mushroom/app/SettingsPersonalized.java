package davis.mushroom.app;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

/**
 * Created by quazeenii on 4/24/14.
 */
public class SettingsPersonalized extends FragmentActivity {

    public ExpandableListView exv;
    private GlobalVariables appState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appState = ((GlobalVariables) getApplicationContext());
        setContentView(R.layout.activity_personal_settings);
        exv = (ExpandableListView) findViewById(R.id.expandableListView);
        exv.setAdapter(new SettingsPersonalizedAdapter(this, AdvancedInputUtils.createParentList(),
                       AdvancedInputUtils.createNestedParentList(), appState));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    public void resetSelection(View view) {
        appState.initializePersonalizedSelectedInput();
        FungusDb fdb = new FungusDb(getApplicationContext());
        fdb.savePersonalizedSettingsToDb();
        for (int i = 0; i < exv.getExpandableListAdapter().getGroupCount(); i++) {
            exv.collapseGroup(i);
        }
    }

    public void backToPersonalizedInput(View view) {
        finish();
    }

}
