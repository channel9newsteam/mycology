package davis.mushroom.app;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

public class BasicInputActivity extends FragmentActivity implements
        LocationListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {
    // Constants
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_VIDEO_CAPTURE = 2;
    private static final int REQUEST_FILE_PICK = 3;

    // Instance Variables
    private boolean mUpdatesRequested;
    private File mediaDirectory;
    private Uri mediaUri;
    private File temp;
    private HashMap<String, String> basicInputMap;

    // XML variables
    private AutoCompleteTextView editType;
    private Button editDate;
    private EditText editCoord;
    private EditText editNotes;
    //private TextView mConnectionState;
    //private TextView mConnectionStatus;

    // Shared Preferences
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefsEditor;

    // Location variables
    private LocationRequest mLocationRequest;
    private LocationClient mLocationClient;

    // Global variables
    private GlobalVariables appState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayAdapter<String> adapter;
        String[] mushroomTypes;
        Time currentDate;

        setContentView(R.layout.activity_basic_input);
        // Assign EditViews to get the data inputted in those fields.
        editType = (AutoCompleteTextView) findViewById(R.id.basic_input_edit_type);
        editDate = (Button) findViewById(R.id.basic_input_edit_date);
        editCoord = (EditText) findViewById(R.id.basic_input_edit_coord);
        editNotes = (EditText) findViewById(R.id.notesSection);

        // Set the autocomplete's resources to the mushroom types found in All that the Rain
        // Promises and More because we are in America.
        mushroomTypes = getResources().getStringArray(R.array.mushroom_types);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mushroomTypes);
        editType.setAdapter(adapter);

        Button clear = (Button) findViewById(R.id.cancel);
        if (!GlobalVariables.showClear) {
            clear.setVisibility(View.INVISIBLE);
        }

        //setup thumbnails and picture taking options
        MediaFunctions.count = 0;
        MediaFunctions.images = new ImageView[8];
        MediaFunctions.images[0] = (ImageView) findViewById(R.id.thumbnail1);
        MediaFunctions.images[1] = (ImageView) findViewById(R.id.thumbnail2);
        MediaFunctions.images[2] = (ImageView) findViewById(R.id.thumbnail3);
        MediaFunctions.images[3] = (ImageView) findViewById(R.id.thumbnail4);
        MediaFunctions.images[4] = (ImageView) findViewById(R.id.thumbnail5);
        MediaFunctions.images[5] = (ImageView) findViewById(R.id.thumbnail6);
        MediaFunctions.images[6] = (ImageView) findViewById(R.id.thumbnail7);
        MediaFunctions.images[7] = (ImageView) findViewById(R.id.thumbnail8);

        appState = ((GlobalVariables) getApplicationContext());
        basicInputMap = appState.selectedInput.get("basicInput");

        setupFolders();
        setupLocation();

       if (basicInputMap.get("Date").equals("")) {
            // Set the date field to be the current date.
            Calendar cal = Calendar.getInstance();
            editDate.setText(new SimpleDateFormat("MMMM dd yyyy").format(cal.getTime()));
            basicInputMap.put("Date", editDate.getText().toString());
       }
        populateDataFromGlobalInputClass();
    }

    private void setupFolders() {
        if (GlobalVariables.savedFolder == null) { // make temp
            cancel(findViewById(R.layout.activity_basic_input));
        } else {
            temp = GlobalVariables.savedFolder;
            mediaDirectory = GlobalVariables.savedMedia;
        }
        MediaFunctions.readIn(this, mediaDirectory);
    }

    private void setupLocation() {
        // Location Client
        // Create a new global location parameters object
        mLocationRequest = LocationRequest.create();

        /*
         * Set the update interval
         */
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        // Note that location updates are off until the user turns them on
        mUpdatesRequested = false;

        // Open Shared Preferences
        mPrefs = getSharedPreferences(MainActivity.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        // Get an editor
        mPrefsEditor = mPrefs.edit();

        //Create a new location client, using the enclosing class to handle callbacks.
        mLocationClient = new LocationClient(this, this, this);
    }

    public void gotoDichotomous(View view) {
        GlobalVariables.setType = true;
        Intent intent = new Intent(this, DichotomousKeyActivity.class);
        startActivity(intent);
    }

    public static class ErrorDialogFragment extends DialogFragment  {
        // Instance variables
        private Dialog mDialog;

        // Default constructor
        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        // Set the dialog to display.
        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        // Return a dialog to the dialog fragment.
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    /**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     */
    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                errorCode,
                this,
                LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            // Create a new DialogFragment in which to show the error dialog
            ErrorDialogFragment errorFragment = new ErrorDialogFragment();

            // Set the dialog in the DialogFragment
            errorFragment.setDialog(errorDialog);

            // Show the error dialog in the DialogFragment
            errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        /*
         * Connect the client. Don't re-start any requests here;
         * instead, wait for onResume()
         */

        if (mLocationClient == null) {
            mLocationClient = new LocationClient(this, this, this);
        }
        mLocationClient.connect();
        populateDataFromGlobalInputClass();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String type;

        populateDataFromGlobalInputClass();

        // If the app already has a setting for getting location updates, get it
        if (mPrefs.contains(LocationUtils.KEY_UPDATES_REQUESTED)) {
            mUpdatesRequested = mPrefs.getBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);
            // Otherwise, turn off location updates until requested
        } else {
            mPrefsEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, false);
            mPrefsEditor.commit();
        }

        if (mPrefs.contains(DichotomousKeyActivity.MUSHROOM_TYPE)) {
            type = mPrefs.getString(DichotomousKeyActivity.MUSHROOM_TYPE, null);

            if (GlobalVariables.setType) {
                editType.setText(GlobalVariables.mushroomType);
            }
        }
        if (!GlobalVariables.setType) {
            GlobalVariables.mushroomType = "";
        }
        editType.setText(GlobalVariables.mushroomType);
        basicInputMap.put("Type", editType.getText().toString());
    }

    private void populateDataFromGlobalInputClass() {
        appState.selectedInput.get("basicInput");
        editDate.setText(basicInputMap.get("Date"));
        editType.setText(basicInputMap.get("Type"));
        editCoord.setText(basicInputMap.get("GPS"));
        editNotes.setText(basicInputMap.get("Notes"));
    }

    @Override
    protected void onPause() {
        // Save the current setting for updates
        mPrefsEditor.putBoolean(LocationUtils.KEY_UPDATES_REQUESTED, mUpdatesRequested);
        mPrefsEditor.commit();

        saveDataToGlobalInputClass();

        super.onPause();
    }

    private void saveDataToGlobalInputClass() {
        basicInputMap.put("Date", editDate.getText().toString());
        basicInputMap.put("Type", editType.getText().toString());
        basicInputMap.put("GPS", editCoord.getText().toString());
        basicInputMap.put("Notes", editNotes.getText().toString());
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mLocationClient.isConnected()) {
            stopPeriodicUpdates();
        }

        mLocationClient.disconnect();
        GlobalVariables.showClear = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GlobalVariables.showClear = true;
        GlobalVariables.unbindDrawables(findViewById(R.id.basic_input_layout));
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        //mConnectionStatus.setText(R.string.connected);

        if (mUpdatesRequested) {
            startPeriodicUpdates();
        }
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
        //mConnectionStatus.setText(R.string.disconnected);
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("onConnectionFailed");
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(
                        this,
                        LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */
            } catch (IntentSender.SendIntentException e) {

                // Log the error
                e.printStackTrace();
            }
        } else {

            // If no resolution is available, display a dialog to the user with the error.
            showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /**
     * Report location updates to the UI.
     *
     * @param location The updated location.
     */
    @Override
    public void onLocationChanged(Location location) {

        // Report to the UI that the location was updated
        //mConnectionStatus.setText(R.string.location_updated);

        // In the UI, set the latitude and longitude to the value received
        editCoord.setText(LocationUtils.getLatLng(this, location));
    }

    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {

        mLocationClient.requestLocationUpdates(mLocationRequest, this);
       //mConnectionState.setText(R.string.location_requested);
    }

    private void stopPeriodicUpdates() {
        mLocationClient.removeLocationUpdates(this);
        //mConnectionState.setText(R.string.location_updates_stopped);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        Intent intent;
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void chooseMedia(View view) {
        if (MediaFunctions.count < 8) {
            final Dialog mediaOptions = new Dialog(this);
            mediaOptions.setContentView(R.layout.options_media);
            mediaOptions.setTitle("Media Options");
            Button chooseFile = (Button) mediaOptions.findViewById(R.id.choose_file);
            chooseFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getFile();
                    mediaOptions.cancel();
                }
            });
            Button choosePhoto = (Button) mediaOptions.findViewById(R.id.choose_photo);
            choosePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    takePhoto();
                    mediaOptions.cancel();
                }
            });
            Button chooseVideo = (Button) mediaOptions.findViewById(R.id.choose_video);
            chooseVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    takeVideo();
                    mediaOptions.cancel();
                }
            });
            mediaOptions.show();
        } else {
            CharSequence text = "You can only have 8 media files. Delete some first.";
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
        }
    }

    public void getFile() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_FILE_PICK);
    }

    public void takePhoto() {
        // takes photo if camera is available
        Context context = getApplicationContext();
        PackageManager pm = context.getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                MediaFunctions.mediaFile = null;
                mediaUri = null;
                try {
                    MediaFunctions.mediaFile = MediaFunctions.createImageFile(mediaDirectory);
                } catch (IOException ex) {
                }
                if (MediaFunctions.mediaFile != null) {
                    mediaUri = Uri.fromFile(MediaFunctions.mediaFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mediaUri);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                }
            } else {
                CharSequence text = "Error: No camera found";
                Toast.makeText(context, text, Toast.LENGTH_LONG).show();
            }

        }
    }

    public void takeVideo() {
        Context context = getApplicationContext();
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(context.getPackageManager()) != null) {
            MediaFunctions.mediaFile = null;
            mediaUri = null;
            try {
                MediaFunctions.mediaFile = MediaFunctions.createVideoFile(mediaDirectory);
                mediaUri = Uri.fromFile(MediaFunctions.mediaFile);
            } catch (IOException ex) {
            }
            if (MediaFunctions.mediaFile != null) {
                takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, mediaUri);
                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if (MediaFunctions.mediaFile == null) {
                MediaFunctions.readIn(this, mediaDirectory);
                Toast.makeText(getApplicationContext(), "There was an error", Toast.LENGTH_SHORT).show();
                return;
            } else {
                MediaFunctions.createThumbnail(this, MediaFunctions.mediaFile);
            }
        } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {
                if (MediaFunctions.mediaFile == null) {
                    MediaFunctions.readIn(this, mediaDirectory);
                    Toast.makeText(getApplicationContext(), "There was an error", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    MediaFunctions.createThumbnail(this, MediaFunctions.mediaFile);
                }
            } else {
                Toast.makeText(getApplicationContext(), "video error", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == REQUEST_FILE_PICK) {
            if (resultCode == RESULT_OK) {
                mediaUri = data.getData();
                String filePath[] = {MediaStore.Images.Media.DATA };
                Cursor c = getContentResolver().query(mediaUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                String picPath = c.getString(columnIndex);
                c.close();
                try {
                    MediaFunctions.mediaFile = MediaFunctions.createImageFile(mediaDirectory);
                    FileOperations.copyFiles(new File(picPath), MediaFunctions.mediaFile);
                    MediaFunctions.createThumbnail(this, MediaFunctions.mediaFile);
                } catch (Exception ex) {
                }
            } else {
                Toast.makeText(getApplicationContext(), "file error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
     * Verify that Google Play services is available before making a request.
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d(LocationUtils.APPTAG, getString(R.string.play_services_available));

            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0);
            if (dialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(dialog);
                errorFragment.show(getSupportFragmentManager(), LocationUtils.APPTAG);
            }
            return false;
        }
    }

    public void getCoordinates(View view) {
        /* Open a new activity to find and keep the coordinates.
         * Check if there is a signal, if no signal then open the map and let the user select the
         * possible location. */
        // If Google Play Services is available
        if (servicesConnected()) {

            // Get the current location
            Location currentLocation = mLocationClient.getLastLocation();

            // Display the current location in the UI
            editCoord.setText(LocationUtils.getLatLng(this, currentLocation));
            basicInputMap.put("GPS", editCoord.getText().toString());
        }
    }

    // TODO: Add checks to the fields to ensure they are valid.
    // If they are not do not add them.
    public void saveBasicInfo(View view) {
        FungusDb fungusDb = new FungusDb(getApplicationContext());
        final CharSequence FIND_SUCCEED = "Find has been saved.";
        Log.i("BasicInputActivity", GlobalVariables.DIR);

        if (GlobalVariables.savedFolder == null) {
            GlobalVariables.currentDateTime = String.valueOf(System.currentTimeMillis()/1000L);
            String currentFileDirectory = GlobalVariables.DIR_HIDDEN + GlobalVariables.currentDateTime;
            GlobalVariables.savedFolder = new File(currentFileDirectory);
            GlobalVariables.savedMedia = new File(GlobalVariables.savedFolder, "Media" + File.separator);
            GlobalVariables.savedMedia.mkdirs();
            temp.renameTo(GlobalVariables.savedFolder);
            mediaDirectory.renameTo(GlobalVariables.savedMedia);
            temp = GlobalVariables.savedFolder;
            mediaDirectory = GlobalVariables.savedMedia;
        } else {
            fungusDb.deleteFindsFromdb("UniqueIdentifier", GlobalVariables.currentDateTime);
        }

        basicInputMap.put("UniqueIdentifier", GlobalVariables.currentDateTime);
        basicInputMap.put("Media", GlobalVariables.savedMedia.getAbsolutePath());
        saveDataToGlobalInputClass();
        fungusDb.saveCurrentFindToDb();
        MediaFunctions.readIn(this, mediaDirectory);

        Toast.makeText(getApplicationContext(), basicInputMap.get("UniqueIdentifier") + " " + FIND_SUCCEED, Toast.LENGTH_LONG)
                .show();

        getOptions();
    }

    public void cancel(View view) {
        editType.getText().clear();
        editDate.setText("Please select a date");
        editCoord.getText().clear();
        editNotes.getText().clear();

        appState = ((GlobalVariables) getApplicationContext());
        appState.initializeSelectedInput();
        basicInputMap = appState.selectedInput.get("basicInput");

        // Set the date field to be the current date.
        Calendar cal = Calendar.getInstance();
        editDate.setText(new SimpleDateFormat("MMMM dd yyyy").format(cal.getTime()));
        basicInputMap.put("Date", editDate.getText().toString());

        // Empty the images files, and set the count back to 0.
        for (ImageView i : MediaFunctions.images) {
            i.setImageBitmap(null);
            i.setImageResource(R.drawable.camera_logo);
            i.setTag(null);
            i.setOnClickListener(null);
        }

        MediaFunctions.count = 0;
        GlobalVariables.savedFolder = null;
        GlobalVariables.savedMedia = null;
        GlobalVariables.currentDateTime = "";
        FileOperations.deleteFiles(GlobalVariables.tempFolder);
        GlobalVariables.mushroomType = "";
        temp = new File(GlobalVariables.DIR_TEMP);
        temp.mkdirs();
        mediaDirectory = new File(temp, "Media" + File.separator);
        mediaDirectory.mkdirs();
        GlobalVariables.setType = false;

        populateDataFromGlobalInputClass();

        //getOptions();
    }

    private void getOptions() {
        final Dialog options = new Dialog(this);
        options.setContentView(R.layout.options_menu);
        options.setTitle("Options");
        Button continued = (Button) options.findViewById(R.id.continue_input);
        continued.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                options.cancel();
            }
        });
        Button main = (Button) options.findViewById(R.id.return_to_main);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                options.cancel();
                finish();
                cancel(v);

            }
        });
        options.show();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
        basicInputMap.put("Date", editDate.getText().toString());
    }
}