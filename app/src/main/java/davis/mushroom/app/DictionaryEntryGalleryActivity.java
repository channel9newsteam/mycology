package davis.mushroom.app;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

public class DictionaryEntryGalleryActivity extends Activity implements
        ViewSwitcher.ViewFactory {

    private static final int REQ_WIDTH = 768;
    private static final int REQ_HEIGHT = 1024;

    private int position;
    private ImageView mViewer;

    private int[] mImageGuideIds = {
        R.drawable.mushroom_cap_morphology,
        R.drawable.mushroom_cap_morphology_cap_under,
        R.drawable.mushroom_cap_morphology_gill,
        R.drawable.mushroom_cap_morphology_cap
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Intent intent;
        String picture;

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_dictionary_entry_gallery);

        mViewer = (ImageView) findViewById(R.id.viewer);

        intent = getIntent();
        picture = intent.getStringExtra(DichotomousKeyActivity.GUIDE);

        position = 0;
        if (picture != null && picture.equals(DichotomousKeyActivity.GUIDE)) {
            Bitmap bm = MediaFunctions
                    .decodeBitmapResourceBounded(getResources(),
                    mImageGuideIds[position], REQ_WIDTH, REQ_HEIGHT);
            // If we view one photo, set the visibility to nothing.

            mViewer.setImageBitmap(bm);
        }
    }

    public View makeView() {
        ImageView i = new ImageView(this);
        i.setBackgroundColor(0xFF000000);
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        return i;
    }

    public void previousPicture(View view) {
        if ((position - 1) >= 0) {
            position--;

            Bitmap bm = MediaFunctions
                    .decodeBitmapResourceBounded(getResources(),
                    mImageGuideIds[position], REQ_WIDTH, REQ_HEIGHT);
            // If we view one photo, set the visibility to nothing.

            mViewer.setImageBitmap(bm);
        }
        else {
            Toast toast = Toast.makeText(this, "No more previous pictures",
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void nextPicture(View view) {
        if ((position + 1) < mImageGuideIds.length) {
            position++;

            Bitmap bm = MediaFunctions
                    .decodeBitmapResourceBounded(getResources(),
                    mImageGuideIds[position], REQ_WIDTH, REQ_HEIGHT);
            // If we view one photo, set the visibility to nothing.

            mViewer.setImageBitmap(bm);
        } else {
            Toast toast = Toast.makeText(this, "No more next pictures",
                    Toast.LENGTH_LONG);
            toast.show();
        }
    }


}