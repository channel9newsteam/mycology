package davis.mushroom.app;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.Stack;


public class DichotomousKeyActivity extends FragmentActivity implements ActionBar.TabListener {
    // Global Constants
    public static final String MUSHROOM_TYPE = "TYPE";
    public static final String GUIDE = "GUIDE";

    private String choice;

    int dichotomyKey; // Location of questionnaire
    Stack tabKey = new Stack(); // Push/pop location for removing tabs

    // Instance Variables
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefsEditor;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_dichotomous_key);

        // Create the adapter that will return a fragment for each of the primary sections
        // of the app.

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home/Up button should be enabled
        actionBar.setHomeButtonEnabled(true);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Setup shared preferences
        mPrefs = getSharedPreferences(MainActivity.SHARED_PREFERENCES, Context.MODE_PRIVATE);

        // Get an editor
        mPrefsEditor = mPrefs.edit();
        mPrefsEditor.putString(MUSHROOM_TYPE, "");
        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.

        // First default tab that asks the gill question
        actionBar.addTab(
                actionBar.newTab()
                        .setText("Question 1")
                        .setTabListener(this));
        setContentView(R.layout.fragment_section_launchpad);
        dichotomyKey = 0;
        tabKey.push(new Integer(0));
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        final ActionBar bar = getActionBar();
        int tabCount = bar.getTabCount();
        int currentPosition = tab.getPosition();
        int duration = Toast.LENGTH_SHORT;

        Context context = getApplicationContext();
        CharSequence text = "You have reselected your tab!";

        if(currentPosition == tabCount -1) {
            // Show that you have selected the current tab
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        }
        else {
            while (tabCount - 1 != currentPosition) {
                // Remove all tabs until it matches the selected tab
                bar.removeTabAt((tabCount--) - 1);
                if(!tabKey.empty()) {
                    Integer a = (Integer) tabKey.pop();
                }
            }
            if(!tabKey.empty()){
                Integer a = (Integer) tabKey.pop();
                dichotomyKey = a;
            } else {
                dichotomyKey = 0;
            }
            setTabView();
        }
    }

    public void onAddTab(View v) {
        final ActionBar bar = getActionBar();
        final int tabCount = bar.getTabCount();
        final String text = "Question " + (tabCount+1);
        Log.v("Dichotomy Key: ", "#" + dichotomyKey);

        setTabView();
        tabKey.push(new Integer(dichotomyKey));

        bar.addTab(bar.newTab()
                .setText(text)
                .setTabListener(this));
        bar.selectTab(bar.getTabAt(tabCount));
    }

    public void onNoTab(View v) {
        switch(dichotomyKey){
            case 0:
                dichotomyKey = 16;
                break;
            case 1:
                dichotomyKey = 3;
                break;
            case 3:
                dichotomyKey = 9;
                break;
            case 4:
                dichotomyKey = 8;
                break;
            case 5:
                dichotomyKey = 7;
                break;
            case 9:
                dichotomyKey = 11;
                break;
            case 11:
                dichotomyKey = 15;
                break;
            case 12:
                dichotomyKey = 14;
                break;
            case 17:
                dichotomyKey = 19;
                break;
            case 16:
                dichotomyKey = 20;
                break;
            case 20:
                dichotomyKey = 22;
                break;
            case 22:
                dichotomyKey = 24;
                break;
            case 24:
                dichotomyKey = 26;
                break;
            case 26:
                dichotomyKey = 28;
                break;
            case 28:
                dichotomyKey = 30;
                break;
            default:
        }
        onAddTab(v);
    }

    public void onYesTab(View v) {
        switch(dichotomyKey) {
            case 0:
                dichotomyKey = 1;
                break;
            case 1:
                dichotomyKey = 2;
                break;
            case 3:
                dichotomyKey = 4;
                break;
            case 4:
                dichotomyKey = 5;
                break;
            case 5:
                dichotomyKey = 6;
                break;
            case 9:
                dichotomyKey = 10;
                break;
            case 11:
                dichotomyKey = 12;
                break;
            case 12:
                dichotomyKey = 13;
                break;
            case 16:
                dichotomyKey = 17;
                break;
            case 17:
                dichotomyKey = 18;
                break;
            case 20:
                dichotomyKey = 21;
                break;
            case 22:
                dichotomyKey = 23;
                break;
            case 24:
                dichotomyKey = 25;
                break;
            case 26:
                dichotomyKey = 27;
                break;
            case 28:
                dichotomyKey = 29;
                break;
            default:
        }
        onAddTab(v);
    }

    public void setTabView() {
        // Change view according to location in key
        switch(dichotomyKey){
            case 0:
                // Gills
                setContentView(R.layout.fragment_section_launchpad);
                break;
            case 1:
                // Pink/yellow
                setContentView(R.layout.activity_dichotomous_key_1);
                break;
            case 2:
                // Amanita
                showResult("Your mushroom is most likely an Amanita!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Amanita));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Amanita);
                break;
            case 3:
                // Veil
                setContentView(R.layout.activity_dichotomous_key_3);
                break;
            case 4:
                // Gills & stalk
                setContentView(R.layout.activity_dichotomous_key_4);
                break;
            case 5:
                // Spores white?
                setContentView(R.layout.activity_dichotomous_key_5);
                break;
            case 6:
                // Misc. light-spored gilled w/ ring
                showResult("Your mushroom is most likely a miscellaneous light-spored " +
                        "gilled mushroom with a ring!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE,
                        getString(R.string.Miscellaneous_Light_spored_Gilled_Mushrooms_with_a_Ring));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Miscellaneous_Light_spored_Gilled_Mushrooms_with_a_Ring);
                break;
            case 7:
                //  Misc. dark-spored gilled
                showResult("Your mushroom is most likely a miscellaneous dark-spored " +
                                "gilled mushroom!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE,
                        getString(R.string.Miscellaneous_Dark_spored_Gilled_Mushrooms));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Miscellaneous_Dark_spored_Gilled_Mushrooms);
                break;
            case 8:
                //  Misc. dark-spored gilled
                showResult("Your mushroom is most likely a Agaricus!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Agaricus));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Agaricus);
                break;
            case 9:
                // Mushroom brittle?
                setContentView(R.layout.activity_dichotomous_key_9);
                break;
            case 10:
                // Milk Caps
                showResult("Your mushroom is most likely a Milk Caps!");
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Milk_Caps_and_Russulas));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Milk_Caps_and_Russulas);
                break;
            case 11:
                // Spores white?
                setContentView(R.layout.activity_dichotomous_key_11);
                break;
            case 12:
                // Gills primitive?
                setContentView(R.layout.activity_dichotomous_key_12);
                break;
            case 13:
                // Chanterelle
                showResult("Your mushroom is most likely a Chanterelle!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Chanterelles));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Chanterelles);
                break;
            case 14:
                // Misc. light-spored gilled w/o ring
                showResult("Your mushroom is most likely a miscellaneous light-spored " +
                        "gilled mushroom without a ring!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE,
                        getString(R.string.Miscellaneous_Light_spored_Gilled_Mushrooms_without_a_Ring));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Miscellaneous_Light_spored_Gilled_Mushrooms_without_a_Ring);
                break;
            case 15:
                // Misc. dark-spored gilled mushroom
                showResult("Your mushroom is most likely a miscellaneous dark-spored " +
                        "gilled mushroom!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE,
                        getString(R.string.Miscellaneous_Dark_spored_Gilled_Mushrooms));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Miscellaneous_Dark_spored_Gilled_Mushrooms);
                break;
            case 16:
                // Under cap pores?
                setContentView(R.layout.activity_dichotomous_key_16);
                break;
            case 17:
                // Fleshy cap and narrow central stalk?
                setContentView(R.layout.activity_dichotomous_key_17);
                break;
            case 18:
                // Boletes
                showResult("Your mushroom is most likely a Boletes!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Bolete));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Bolete);
                break;
            case 19:
                // Polypores
                showResult("Your mushroom is most likely a Polypores!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Polypores));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Polypores);
                break;
            case 20:
                // Downward pointy spine?
                setContentView(R.layout.activity_dichotomous_key_20);
                break;
            case 21:
                // Tooth fungi
                showResult("Your mushroom is most likely a Teeth Fungi");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Teeth_Fungi));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Teeth_Fungi);
                break;
            case 22:
                // Cap brainlike?
                setContentView(R.layout.activity_dichotomous_key_22);
                break;
            case 23:
                // Morsels
                showResult("Your mushroom is most likely a Morels & False Morels!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Morels_and_False_Morels));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Morels_and_False_Morels);
                break;
            case 24:
                // Clublike or corral?
                setContentView(R.layout.activity_dichotomous_key_24);
                break;
            case 25:
                // Club & corral fungi
                showResult("Your mushroom is most likely a Coral & Club Fungi!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Coral_and_Club_Fungi));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Coral_and_Club_Fungi);
                break;
            case 26:
                // Spore case?
                setContentView(R.layout.activity_dichotomous_key_26);
                break;
            case 27:
                // Puffballs & earthstars
                showResult("Your mushroom is most likely a Puffball or Earthstar!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Puffballs_and_Earthstars));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Puffballs_and_Earthstars);
                break;
            case 28:
                // Cap and stalk jellylike?
                setContentView(R.layout.activity_dichotomous_key_28);
                break;
            case 29:
                // Chanterelle
                showResult("Your mushroom is most likely a Chanterelle!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Chanterelles));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Chanterelles);

                break;
            case 30:
                // Other
                showResult("Your mushroom is a unique mushroom!");

                // Write the type to the shared preferences file for BasicInputActivity.
                mPrefsEditor.putString(MUSHROOM_TYPE, getString(R.string.Other_Mushroom));
                mPrefsEditor.commit();
                GlobalVariables.mushroomType = getString(R.string.Other_Mushroom);

                break;
            default:

        }
    }

    public void showResult(String text) {
        new AlertDialog.Builder(this)
                .setTitle(text)
//                .setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialogInterface, int item) {
//                        giveResult(items[item]);
//                    }
//                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(DichotomousKeyActivity.this, DictionaryEntryActivity.class);
                        Bundle extras = new Bundle();
                        extras.putString("DictionaryExpandableListAdapter.Type", GlobalVariables.mushroomType);
                        extras.putString("DictionaryExpandableListAdapter.Index", String.valueOf(0));
                        intent.putExtras(extras);
                        startActivity(intent);
                        // Return to home screen
                        finish();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create()
                .show();
        return;
    }

    private void giveResult(String clicked) {
        choice = clicked;
        Log.v("Value: ",choice);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.\

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            // Handle presses on the action bar items
            case R.id.action_settings:
                showPopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showPopup() {
        View menuItemView = findViewById(R.id.action_settings);
        PopupMenu popup = new PopupMenu(this, menuItemView);
        MenuInflater inflate = popup.getMenuInflater();
        inflate.inflate(R.menu.dichotomous_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch(item.getItemId()) {
                    case R.id.About:
                        //Display a dialog about the ap
                        DialogFragment newFragment = new AboutDialog();
                        newFragment.show(getFragmentManager(), "AboutTag");
                        return true;
                    case R.id.Guide:
                        viewDichotomousGuide();
                        return true;
                    default:
                }
                return false;
            }
        });
        popup.show();
    }

    private void viewDichotomousGuide() {
        Intent intent = new Intent(this, DictionaryEntryGalleryActivity.class);

        intent.putExtra(GUIDE, GUIDE);
        startActivity(intent);
    }
}
