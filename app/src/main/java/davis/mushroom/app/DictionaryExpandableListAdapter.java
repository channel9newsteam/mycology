package davis.mushroom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Garrett on 5/19/14.
 */
public class DictionaryExpandableListAdapter extends BaseExpandableListAdapter {
    public static final String MUSHROOM_TYPE = "DictionaryExpandableListAdapter.Type";
    public static final String DICTIONARY_INDEX = "DictionaryExpandableListAdapter.Index";

    private Context context;
    private LayoutInflater inflater;
    private ArrayList<String> parentList;
    private ArrayList<ArrayList<String>> childList;


    public DictionaryExpandableListAdapter(Context context, ArrayList<String> parentList, ArrayList<ArrayList<String>> childList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.parentList = parentList;
        this.childList = childList;
    }

    @Override
    public int getGroupCount() {
        return parentList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childList.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupPosition;
    }

    @Override
    public Object getChild(int i, int i2) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int i, int i2) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean b, View view, ViewGroup viewGroup) {
        TextView tv = new TextView(context);
        tv.setText(parentList.get(groupPosition));
        tv.setPadding(50, 10, 10, 10);
        tv.setTextSize(26);

        return tv;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, final boolean b, final View view, final ViewGroup viewGroup) {
        final View v;
        if (view != null) {
            v = view;
        } else {
            v = inflater.inflate(R.layout.activity_dictionary_child, viewGroup, false);
        }

        final TextView tv = (TextView) v.findViewById(R.id.childname);


        tv.setText(childList.get(groupPosition).get(childPosition));

        //checks to see if they clicked the text
        tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //take the user to the dictionary pages here
                String mushroomType = parentList.get(groupPosition);

                Intent intent = new Intent(context, DictionaryEntryActivity.class);
                Bundle extras = new Bundle();
                extras.putString(MUSHROOM_TYPE, mushroomType);
                extras.putString(DICTIONARY_INDEX, String.valueOf(childPosition));
                intent.putExtras(extras);
                context.startActivity(intent);
            }
        });



        return v;
    }

    @Override
    public boolean isChildSelectable(int i, int i2) {
        return true;
    }

}
